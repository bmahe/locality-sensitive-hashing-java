package net.bmahe.lsh.core;

import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang3.Validate;

import net.bmahe.lsh.core.hashfunctions.GroupHashFunctionsFactory;

public class UnorderedLSH extends LSH {

	public UnorderedLSH(final GroupHashFunctionsFactory grouphashFunctionsFactory,
			final LocalSensitiveFunction localSensitiveFunction, final int numBands, final int numBuckets) {
		super(grouphashFunctionsFactory, localSensitiveFunction, numBands, numBuckets);
	}

	public int[] hash(final Set<Integer> document) {
		Objects.requireNonNull(document);
		Validate.isTrue(document.size() > 0);

		final int[] docArr = new int[document.size()];
		int idx = 0;
		for (final Integer docEntry : document) {
			docArr[idx] = docEntry;
			idx++;
		}

		return hash(docArr);
	}
}