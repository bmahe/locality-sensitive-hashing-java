package net.bmahe.lsh.core;

public interface LocalSensitiveFunction {

	int[] hash(final int[] document);

	int[] hashSparse(final int[] indices, final int[] values);

	int[] hash(final double[] document);

	int[] hashSparse(final int[] indices, final double[] values);
	
	int[] hash(final float[] document);

	int[] hashSparse(final int[] indices, final float[] values);

}