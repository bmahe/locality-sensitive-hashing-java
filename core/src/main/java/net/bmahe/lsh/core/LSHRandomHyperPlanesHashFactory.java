package net.bmahe.lsh.core;

import java.util.Random;

import org.apache.commons.lang3.Validate;

import net.bmahe.lsh.core.hashfunctions.GroupHashFunctionsFactory;
import net.bmahe.lsh.core.hashfunctions.LCGGroupHashFunctionsFactory;
import net.bmahe.lsh.core.hashfunctions.LCGHashGenerator;

public class LSHRandomHyperPlanesHashFactory {

	public static LSH build(final int inputSize, final int hashSize, final int numBands, final int numBuckets,
			final Random random) {
		Validate.notNull(random);
		Validate.isTrue(inputSize > 0);
		Validate.isTrue(hashSize > 0);
		Validate.isTrue(numBands > 1);
		Validate.isTrue(numBuckets > 1);

		final LCGHashGenerator lcgHashGenerator = new LCGHashGenerator(random);
		final GroupHashFunctionsFactory grouphashFunctionsFactory = new LCGGroupHashFunctionsFactory(lcgHashGenerator);

		final RandomHyperPlanesHash randomHyperPlanesHash = new RandomHyperPlanesHash(inputSize, hashSize, random);
		final LSH lshRandomHyperPlanes = new LSH(grouphashFunctionsFactory, randomHyperPlanesHash, numBands, numBuckets);

		return lshRandomHyperPlanes;
	}

	public static LSH build(final int inputSize, final int hashSize, final int numBands, final int numBuckets,
			final long seed) {
		return build(inputSize, hashSize, numBands, numBuckets, new Random(seed));
	}
}