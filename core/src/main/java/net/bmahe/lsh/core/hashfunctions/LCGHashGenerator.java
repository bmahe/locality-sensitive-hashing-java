package net.bmahe.lsh.core.hashfunctions;

import java.util.Objects;
import java.util.Random;

/**
 * Based on Linear Congruential Generator <br>
 * The generated hash function is in the form of: <br>
 * h(x) = (a*x + b) mod p <br>
 * Where: <br>
 * - a and b a randomly chosen numbers modulo p where a != 0 <br>
 *
 * @see <a href="https://en.wikipedia.org/wiki/Universal_hashing">Wikipedia -
 *      Universal Hashing</a>
 * @author bruno
 *
 */
public class LCGHashGenerator implements HashGenerator<LCGHashFunction> {

	/**
	 * 2**31 - 1
	 */
	public final static int DEFAULT_P = Integer.MAX_VALUE;

	private final Random random;

	public LCGHashGenerator(final Random _random) {
		Objects.requireNonNull(_random);

		this.random = _random;
	}

	public Random getRandom() {
		return random;
	}

	public int getP() {
		return DEFAULT_P;
	}

	@Override
	public LCGHashFunction generate() {
		final int a = random.nextInt(getP()) + 1;
		final int b = random.nextInt(getP()) + 1;

		return new LCGHashFunction(getP(), a, b);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((random == null) ? 0 : random.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LCGHashGenerator other = (LCGHashGenerator) obj;
		if (random == null) {
			if (other.random != null)
				return false;
		} else if (!random.equals(other.random))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "LCGUniversalHashGenerator [random=" + random + ", p=" + getP() + "]";
	}

}