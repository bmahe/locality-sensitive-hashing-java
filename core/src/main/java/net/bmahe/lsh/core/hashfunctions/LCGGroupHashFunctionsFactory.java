package net.bmahe.lsh.core.hashfunctions;

import org.apache.commons.lang3.Validate;

public class LCGGroupHashFunctionsFactory implements GroupHashFunctionsFactory {

	private final LCGHashGenerator lcgHashGenerator;

	public LCGGroupHashFunctionsFactory(final LCGHashGenerator _lcgHashGenerator) {
		Validate.notNull(_lcgHashGenerator);

		this.lcgHashGenerator = _lcgHashGenerator;
	}

	@Override
	public GroupHashFunctions build(final int hashSize) {
		Validate.isTrue(hashSize > 0);

		return new LCGGroupHashFunctions(hashSize, lcgHashGenerator);
	}
}