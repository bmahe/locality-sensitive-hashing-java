package net.bmahe.lsh.core;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

import org.apache.commons.lang3.Validate;

/**
 * TODO refactor to avoid duplication
 * 
 * @author bruno
 *
 */
public class RandomHyperPlanesHash implements LocalSensitiveFunction {

	final private int inputSize;
	final private int hashSize;
	final private Random random;

	final private double[][] hyperPlaneVectors;

	public RandomHyperPlanesHash(final int _inputSize, final int _hashSize, final Random _random) {
		Validate.isTrue(_inputSize > 0);
		Validate.isTrue(_hashSize > 0);
		Objects.requireNonNull(_random);

		this.inputSize = _inputSize;
		this.hashSize = _hashSize;
		this.random = _random;

		hyperPlaneVectors = new double[hashSize][inputSize];
		initHyperPlanes();
	}

	private void initHyperPlanes() {
		for (int hyperPlaneIdx = 0; hyperPlaneIdx < hashSize; hyperPlaneIdx++) {
			for (int vectorIdx = 0; vectorIdx < inputSize; vectorIdx++) {
				hyperPlaneVectors[hyperPlaneIdx][vectorIdx] = random.nextDouble() >= 0.5 ? 1.0 : -1.0;
			}
		}
	}

	public int getInputSize() {
		return inputSize;
	}

	public int getHashSize() {
		return hashSize;
	}

	public Random getRandom() {
		return random;
	}

	public double[][] getHyperPlaneVectors() {
		return hyperPlaneVectors;
	}

	@Override
	public int[] hash(final int[] document) {
		Objects.requireNonNull(document);
		Validate.isTrue(document.length == inputSize);

		final int hash[] = new int[hashSize];

		for (int hashIdx = 0; hashIdx < hashSize; hashIdx++) {
			for (int docIdx = 0; docIdx < document.length; docIdx++) {
				hash[hashIdx] += hyperPlaneVectors[hashIdx][docIdx] * document[docIdx];
			}

			if (hash[hashIdx] >= 0) {
				hash[hashIdx] = 1;
			} else {
				hash[hashIdx] = -1;
			}
		}

		return hash;
	}

	@Override
	public int[] hash(final double[] document) {
		Objects.requireNonNull(document);
		Validate.isTrue(document.length == inputSize);

		final int hash[] = new int[hashSize];

		for (int hashIdx = 0; hashIdx < hashSize; hashIdx++) {
			for (int docIdx = 0; docIdx < document.length; docIdx++) {
				hash[hashIdx] += hyperPlaneVectors[hashIdx][docIdx] * document[docIdx];
			}

			if (hash[hashIdx] >= 0) {
				hash[hashIdx] = 1;
			} else {
				hash[hashIdx] = -1;
			}
		}

		return hash;
	}

	@Override
	public int[] hash(final float[] document) {
		Objects.requireNonNull(document);
		Validate.isTrue(document.length == inputSize);

		final int hash[] = new int[hashSize];

		for (int hashIdx = 0; hashIdx < hashSize; hashIdx++) {
			for (int docIdx = 0; docIdx < document.length; docIdx++) {
				hash[hashIdx] += hyperPlaneVectors[hashIdx][docIdx] * document[docIdx];
			}

			if (hash[hashIdx] >= 0) {
				hash[hashIdx] = 1;
			} else {
				hash[hashIdx] = -1;
			}
		}

		return hash;
	}

	@Override
	public int[] hashSparse(int[] indices, int[] values) {
		Objects.requireNonNull(indices);
		Objects.requireNonNull(values);
		Validate.isTrue(indices.length > 0);
		Validate.isTrue(indices.length == values.length);

		final int hash[] = new int[hashSize];

		for (int hashIdx = 0; hashIdx < hashSize; hashIdx++) {
			for (int docIdx = 0; docIdx < indices.length; docIdx++) {
				final int vectorIdx = indices[docIdx];
				hash[hashIdx] += hyperPlaneVectors[hashIdx][vectorIdx] * values[docIdx];
			}

			if (hash[hashIdx] >= 0) {
				hash[hashIdx] = 1;
			} else {
				hash[hashIdx] = -1;
			}
		}

		return hash;
	}

	@Override
	public int[] hashSparse(int[] indices, double[] values) {
		Objects.requireNonNull(indices);
		Objects.requireNonNull(values);
		Validate.isTrue(indices.length > 0);
		Validate.isTrue(indices.length == values.length);

		final int hash[] = new int[hashSize];

		for (int hashIdx = 0; hashIdx < hashSize; hashIdx++) {
			for (int docIdx = 0; docIdx < indices.length; docIdx++) {
				final int vectorIdx = indices[docIdx];
				hash[hashIdx] += hyperPlaneVectors[hashIdx][vectorIdx] * values[docIdx];
			}

			if (hash[hashIdx] >= 0) {
				hash[hashIdx] = 1;
			} else {
				hash[hashIdx] = -1;
			}
		}

		return hash;
	}

	@Override
	public int[] hashSparse(int[] indices, float[] values) {
		Objects.requireNonNull(indices);
		Objects.requireNonNull(values);
		Validate.isTrue(indices.length > 0);
		Validate.isTrue(indices.length == values.length);

		final int hash[] = new int[hashSize];

		for (int hashIdx = 0; hashIdx < hashSize; hashIdx++) {
			for (int docIdx = 0; docIdx < indices.length; docIdx++) {
				final int vectorIdx = indices[docIdx];
				hash[hashIdx] += hyperPlaneVectors[hashIdx][vectorIdx] * values[docIdx];
			}

			if (hash[hashIdx] >= 0) {
				hash[hashIdx] = 1;
			} else {
				hash[hashIdx] = -1;
			}
		}

		return hash;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + hashSize;
		result = prime * result + Arrays.deepHashCode(hyperPlaneVectors);
		result = prime * result + inputSize;
		result = prime * result + ((random == null) ? 0 : random.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RandomHyperPlanesHash other = (RandomHyperPlanesHash) obj;
		if (hashSize != other.hashSize)
			return false;
		if (!Arrays.deepEquals(hyperPlaneVectors, other.hyperPlaneVectors))
			return false;
		if (inputSize != other.inputSize)
			return false;
		if (random == null) {
			if (other.random != null)
				return false;
		} else if (!random.equals(other.random))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RandomHyperPlanesHash [inputSize=" + inputSize + ", hashSize=" + hashSize + ", random=" + random
				+ ", hyperPlaneVectors=" + Arrays.toString(hyperPlaneVectors) + "]";
	}
}