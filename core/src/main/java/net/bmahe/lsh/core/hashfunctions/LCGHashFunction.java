package net.bmahe.lsh.core.hashfunctions;

import org.apache.commons.lang3.Validate;

public final class LCGHashFunction implements HashFunction {

	final private int p;
	final private long a;
	final private long b;

	LCGHashFunction(final int _p, final long _a, final long _b) {
		Validate.isTrue(_p > 0);
		Validate.isTrue(_a > 0);
		Validate.isTrue(_b > 0);

		this.p = _p;
		this.a = _a;
		this.b = _b;

	}

	public int getP() {
		return p;
	}

	public long getA() {
		return a;
	}

	public long getB() {
		return b;
	}

	public Integer apply(final long x) {
		return (int) ((x * a + b) % p);
	}

	@Override
	public Integer apply(final Integer x) {
		return apply((long) x);
	}

	@Override
	public int[] apply(final int[] input) {
		Validate.isTrue(input.length > 0);

		int[] hashes = new int[input.length];

		for (int i = 0; i < input.length; i++) {
			hashes[i] = (int) ((input[i] * a + b) % p);
		}

		return hashes;
	}

	@Override
	public void apply(final int[] input, final int[] output, final int length) {
		Validate.isTrue(input.length > 0);
		Validate.isTrue(input.length >= length);
		Validate.isTrue(input.length <= output.length);

		for (int i = 0; i < length; i++) {
			output[i] = (int) ((input[i] * a + b) % p);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (a ^ (a >>> 32));
		result = prime * result + (int) (b ^ (b >>> 32));
		result = prime * result + p;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LCGHashFunction other = (LCGHashFunction) obj;
		if (a != other.a)
			return false;
		if (b != other.b)
			return false;
		if (p != other.p)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "LCGHashFunction [p=" + p + ", a=" + a + ", b=" + b + "]";
	}

}