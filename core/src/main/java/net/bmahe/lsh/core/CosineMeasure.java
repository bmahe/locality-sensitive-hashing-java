package net.bmahe.lsh.core;

import org.apache.commons.lang3.Validate;

public class CosineMeasure {

	/**
	 * Private constructor as this class offers purely static methods
	 */
	private CosineMeasure() {
	}

	public static double cosineSimilarityHash(final int[] hash1, final int[] hash2) {
		Validate.notNull(hash1);
		Validate.notNull(hash2);
		Validate.isTrue(hash1.length == hash2.length);

		if (hash1.length == 0) {
			return 0.0;
		}

		int numEntriesSame = 0;
		for (int i = 0; i < hash1.length; i++) {
			if (hash1[i] == hash2[i]) {
				numEntriesSame += 1;
			}
		}

		final double ratioSame = (double) numEntriesSame / hash1.length;
		return Math.cos(Math.PI * (1.0 - ratioSame));
	}
}