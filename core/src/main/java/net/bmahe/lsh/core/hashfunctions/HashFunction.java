package net.bmahe.lsh.core.hashfunctions;

import java.util.function.Function;

public interface HashFunction extends Function<Integer, Integer> {

	int[] apply(int[] input);

	void apply(int[] input, int[] output, int length);
}