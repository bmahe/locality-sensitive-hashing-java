package net.bmahe.lsh.core.hashfunctions;

import java.util.Objects;

import org.apache.commons.lang3.Validate;

public class LCGGroupHashFunctions implements GroupHashFunctions {

	private final LCGHashGenerator lcgHashGenerator;
	private final int hashSize;

	private final long[][] coefficients;
	private final LCGHashFunction[] lcgHashFunctions;

	public LCGGroupHashFunctions(final int _hashSize, final LCGHashGenerator _lcgHashGenerator) {
		Validate.isTrue(_hashSize > 0);
		Objects.requireNonNull(_lcgHashGenerator);

		this.hashSize = _hashSize;
		this.lcgHashGenerator = _lcgHashGenerator;

		this.coefficients = new long[hashSize][2];
		this.lcgHashFunctions = new LCGHashFunction[hashSize];
		for (int i = 0; i < _hashSize; i++) {
			final LCGHashFunction hashFunction = lcgHashGenerator.generate();

			this.lcgHashFunctions[i] = hashFunction;
			this.coefficients[i][0] = hashFunction.getA();
			this.coefficients[i][1] = hashFunction.getB();
		}
	}

	@Override
	public int getNumHashFunctions() {
		return hashSize;
	}

	public LCGHashFunction getHashFunction(final int i) {
		Validate.isTrue(i >= 0);
		Validate.isTrue(i < hashSize);

		return lcgHashFunctions[i];
	}

	@Override
	public int apply(final int hashFunctionId, final int i) {
		Validate.isTrue(hashFunctionId >= 0 && hashFunctionId < hashSize);

		return (int) ((coefficients[hashFunctionId][0] * i + coefficients[hashFunctionId][1])
				% lcgHashGenerator.getP());
	}

}