package net.bmahe.lsh.core;

import java.util.Arrays;
import java.util.Objects;

import org.apache.commons.lang3.Validate;

import net.bmahe.lsh.core.hashfunctions.GroupHashFunctions;
import net.bmahe.lsh.core.hashfunctions.GroupHashFunctionsFactory;

public class MinHash implements LocalSensitiveFunction {

	private final GroupHashFunctionsFactory groupHashFunctionsFactory;

	private final int hashSize;
	private final GroupHashFunctions groupHashFunctions;

	/**
	 * Constructor for MinHash implementation <br>
	 * <br>
	 * <ul>
	 * <li>The seed used for the GroupHashFunctionsFactory needs to be identical
	 * </ul>
	 * 
	 * @param _hashSize                  Size of the hash to compute
	 * @param _groupHashFunctionsFactory Used for generating hashing functions
	 */
	public MinHash(final int _hashSize, final GroupHashFunctionsFactory _groupHashFunctionsFactory) {
		Validate.isTrue(_hashSize > 0);
		Objects.requireNonNull(_groupHashFunctionsFactory);

		this.hashSize = _hashSize;
		this.groupHashFunctionsFactory = _groupHashFunctionsFactory;
		this.groupHashFunctions = groupHashFunctionsFactory.build(hashSize);

	}

	public int getHashSize() {
		return hashSize;
	}

	public int[] hash(final int[] document) {
		Objects.requireNonNull(document);
		Validate.isTrue(document.length > 0);

		final int[] computedHash = new int[hashSize];

		// Initialize to infinity
		Arrays.fill(computedHash, Integer.MAX_VALUE);

		for (int docIdx = 0; docIdx < document.length; docIdx++) {
			int k = document[docIdx];

			for (int i = 0; i < hashSize; i++) {
				final int hashed = groupHashFunctions.apply(i, k);
				computedHash[i] = Math.min(computedHash[i], hashed);
			}
		}
		return computedHash;
	}

	@Override
	public int[] hashSparse(final int[] indices, final int[] values) {
		Objects.requireNonNull(indices);
		Objects.requireNonNull(values);
		Validate.isTrue(indices.length > 0);
		Validate.isTrue(indices.length == values.length);

		/**
		 * Order is irrelevant
		 */
		return hash(values);
	}

	@Override
	public int[] hash(final double[] document) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int[] hashSparse(final int[] indices, final double[] values) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int[] hash(final float[] document) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int[] hashSparse(final int[] indices, final float[] values) {
		throw new UnsupportedOperationException();
	}
}