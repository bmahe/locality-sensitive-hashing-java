package net.bmahe.lsh.core.hashfunctions;

public interface GroupHashFunctions {

	int getNumHashFunctions();
	int apply(final int hashFunctionId, final int i);
}