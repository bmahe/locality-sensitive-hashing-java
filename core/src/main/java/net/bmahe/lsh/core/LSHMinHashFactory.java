package net.bmahe.lsh.core;

import java.util.Objects;
import java.util.Random;

import org.apache.commons.lang3.Validate;

import net.bmahe.lsh.core.hashfunctions.GroupHashFunctionsFactory;
import net.bmahe.lsh.core.hashfunctions.LCGGroupHashFunctionsFactory;
import net.bmahe.lsh.core.hashfunctions.LCGHashGenerator;

public class LSHMinHashFactory {

	private final static double THRESHOLD = 0.5;
	private final static double LOG_THRESHOLD = Math.log(THRESHOLD);

	/**
	 * Compute the minhash size based on the number of bands. <br>
	 * As we know: <br>
	 * <ul>
	 * <li>n = r * b</li>
	 * <li>t = (1 / b) ^ (1 / r)</li>
	 * </ul>
	 * <br>
	 * Where: <br>
	 * <ul>
	 * <li>n is the total number of rows</li>
	 * <li>b is the number of bands</li>
	 * <li>r is the number of rows per band</li>
	 * <li>t is the value of similarity s at which the probability of becoming a
	 * candidate is 1/2. Being a candidate means all the rows of one band match</li>
	 * </ul>
	 * <br>
	 * We can then express n as a function of r, b and t. t is known to be 0.5 by
	 * definition. Reasoning: <br>
	 * <ul>
	 * <li>t = (1 / b) ^ (1 / r)</li>
	 * <li>We also know that n = r * b and therefore r = n / b. We can use that to
	 * replace r in the previous equation</li>
	 * <li>t = (1 / b) ^ (b / n)</li>
	 * <li>ln(t) = (b / n) * ln(1 / b)</li>
	 * <li>n * ln(t) = b * ln(1 / b)</li>
	 * <li>n = b * ln(1 / b) / ln(t)</li>
	 * </ul>
	 * 
	 * @see <a href="http://www.mmds.org/">Mining of Massive Datasets - Chapter
	 *      3</a>
	 * 
	 * @param numBands Number of bands
	 * @return An appropriate size for the hash
	 */

	public static int computeHashSize(final int numBands) {
		Validate.isTrue(numBands > 1);

		return (int) Math.ceil(numBands * Math.log(1. / numBands) / LOG_THRESHOLD);

	}

	public static int computeNumBands(final int numRows, final double threshold) {
		Validate.isTrue(numRows >= 1);
		Validate.isTrue(threshold >= 0.0);
		Validate.isTrue(threshold <= 1.0);

		return (int) Math.ceil(Math.pow(1.0 / threshold, numRows));
	}

	public static UnorderedLSH build(final int numRows, final double threshold, final int numBuckets,
			final Random random) {
		Objects.requireNonNull(random);
		Validate.isTrue(numRows >= 1);
		Validate.isTrue(threshold >= 0.0);
		Validate.isTrue(threshold <= 1.0);
		Validate.isTrue(numBuckets > 1);

		final LCGHashGenerator lcgHashGenerator = new LCGHashGenerator(random);
		final GroupHashFunctionsFactory grouphashFunctionsFactory = new LCGGroupHashFunctionsFactory(lcgHashGenerator);

		final int numBands = computeNumBands(numRows, threshold);
		final int hashSize = numBands * numRows;

		final MinHash minHash = new MinHash(hashSize, grouphashFunctionsFactory);
		return new UnorderedLSH(grouphashFunctionsFactory, minHash, numBands, numBuckets);
	}

	public static UnorderedLSH build(final int numBands, final int numBuckets, final Random random) {
		Objects.requireNonNull(random);
		Validate.isTrue(numBands > 1);
		Validate.isTrue(numBuckets > 1);

		final LCGHashGenerator lcgHashGenerator = new LCGHashGenerator(random);
		final GroupHashFunctionsFactory grouphashFunctionsFactory = new LCGGroupHashFunctionsFactory(lcgHashGenerator);

		final int hashSize = computeHashSize(numBands);

		final MinHash minHash = new MinHash(hashSize, grouphashFunctionsFactory);
		return new UnorderedLSH(grouphashFunctionsFactory, minHash, numBands, numBuckets);
	}

	public static UnorderedLSH build(final int numBands, final int numBuckets, final long seed) {
		return build(numBands, numBuckets, new Random(seed));
	}

	public static UnorderedLSH build(final int numRows, final double threshold, final int numBuckets, final long seed) {
		return build(numRows, threshold, numBuckets, new Random(seed));
	}
}