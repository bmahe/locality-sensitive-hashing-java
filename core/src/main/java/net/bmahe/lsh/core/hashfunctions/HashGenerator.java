package net.bmahe.lsh.core.hashfunctions;

public interface HashGenerator<T extends HashFunction> {

	T generate();
}