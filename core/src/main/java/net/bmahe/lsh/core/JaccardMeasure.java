package net.bmahe.lsh.core;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.Validate;

public class JaccardMeasure {

	/**
	 * Private constructor as this class offers purely static methods
	 */
	private JaccardMeasure() {

	}

	public static <T> double jaccardSimilarity(Set<T> document1, Set<T> document2) {
		Validate.notNull(document1);
		Validate.notNull(document2);

		final Set<T> intersection = new HashSet<>();

		if (document1.size() < document2.size()) {
			intersection.addAll(document1);
			intersection.retainAll(document2);
		} else {
			intersection.addAll(document2);
			intersection.retainAll(document1);
		}

		final Set<T> union = new HashSet<>();
		union.addAll(document1);
		union.addAll(document2);

		if (union.size() == 0) {
			return 0.0;
		}

		return (double) intersection.size() / union.size();
	}

	public static <T> double jaccardDistance(Set<T> document1, Set<T> document2) {
		return 1.0 - jaccardSimilarity(document1, document2);
	}

	public static double jaccardSimilarityHash(int[] hash1, int[] hash2) {
		Validate.notNull(hash1);
		Validate.notNull(hash2);
		Validate.isTrue(hash1.length == hash2.length);

		if (hash1.length == 0) {
			return 0.0;
		}

		double intersection = 0.0;
		for (int i = 0; i < hash1.length; i++) {
			if (hash1[i] == hash2[i]) {
				intersection += 1.0;
			}
		}

		return intersection / hash1.length;
	}

	public static double jaccardDistanceHash(int[] hash1, int[] hash2) {
		return 1.0 - jaccardSimilarityHash(hash1, hash2);
	}
}