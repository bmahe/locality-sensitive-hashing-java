package net.bmahe.lsh.core.hashfunctions;

public interface GroupHashFunctionsFactory {

	GroupHashFunctions build(int hashSize);
}