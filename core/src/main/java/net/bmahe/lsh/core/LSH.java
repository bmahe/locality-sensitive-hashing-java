package net.bmahe.lsh.core;

import java.util.Objects;

import org.apache.commons.lang3.Validate;

import net.bmahe.lsh.core.hashfunctions.GroupHashFunctions;
import net.bmahe.lsh.core.hashfunctions.GroupHashFunctionsFactory;

public class LSH {

	private final GroupHashFunctionsFactory grouphashFunctionsFactory;
	private final LocalSensitiveFunction localSensitiveFunctionminHash;
	private final int numBands;
	private final int numBuckets;

	private final GroupHashFunctions hashFunctions;

	public LSH(final GroupHashFunctionsFactory _grouphashFunctionsFactory,
			final LocalSensitiveFunction _localSensitiveFunction, final int _numBands, final int _numBuckets) {
		Objects.requireNonNull(_grouphashFunctionsFactory);
		Objects.requireNonNull(_localSensitiveFunction);
		Validate.isTrue(_numBands > 0);
		Validate.isTrue(_numBuckets > 0);

		this.grouphashFunctionsFactory = _grouphashFunctionsFactory;
		this.localSensitiveFunctionminHash = _localSensitiveFunction;
		this.numBands = _numBands;
		this.numBuckets = _numBuckets;

		hashFunctions = this.grouphashFunctionsFactory.build(1);
	}

	protected int[] hash2Buckets(final int[] hash) {
		Objects.requireNonNull(hash);
		Validate.isTrue(hash.length > 0);

		final int[] lsh = new int[numBands];

		int currentBand = 0;
		for (int i = 0; i < hash.length; i++) {

			lsh[currentBand] = (int) ((long) lsh[currentBand] + (long) hashFunctions.apply(0, hash[i])) % numBuckets;

			currentBand = (currentBand + 1) % numBands;
		}

		return lsh;
	}

	public int[] hash(final int[] document) {
		Objects.requireNonNull(document);
		Validate.isTrue(document.length > 0);

		final int[] documentHash = localSensitiveFunctionminHash.hash(document);

		return hash2Buckets(documentHash);
	}

	public int[] hash(final double[] document) {
		Objects.requireNonNull(document);
		Validate.isTrue(document.length > 0);

		final int[] documentHash = localSensitiveFunctionminHash.hash(document);

		return hash2Buckets(documentHash);
	}

	public int[] hash(final float[] document) {
		Objects.requireNonNull(document);
		Validate.isTrue(document.length > 0);

		final int[] documentHash = localSensitiveFunctionminHash.hash(document);

		return hash2Buckets(documentHash);
	}

	public LocalSensitiveFunction getMinHash() {
		return localSensitiveFunctionminHash;
	}

	public int getNumBands() {
		return numBands;
	}

	public int getNumBuckets() {
		return numBuckets;
	}
}