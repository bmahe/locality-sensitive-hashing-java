package net.bmahe.lsh.core.hashfunctions;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Random;

import org.junit.jupiter.api.Test;

public class LCGHashFunctionTest {

	@Test
	public void noNegativeP() {
		assertThrows(IllegalArgumentException.class, () -> new LCGHashFunction(-1, 1, 1));
	}

	@Test
	public void noZeroP() {
		assertThrows(IllegalArgumentException.class, () -> new LCGHashFunction(0, 1, 1));
	}

	@Test
	public void noNegativeA() {
		assertThrows(IllegalArgumentException.class, () -> new LCGHashFunction(1, -1, 1));
	}

	@Test
	public void noZeroA() {
		assertThrows(IllegalArgumentException.class, () -> new LCGHashFunction(1, 0, 1));
	}

	@Test
	public void noNegativeB() {
		assertThrows(IllegalArgumentException.class, () -> new LCGHashFunction(1, 1, -1));
	}

	@Test
	public void noZeroB() {
		assertThrows(IllegalArgumentException.class, () -> new LCGHashFunction(1, 1, 0));
	}

	@Test
	public void invalidApplyInputArray() {
		final LCGHashFunction lcgHashFunction = new LCGHashFunction(1, 2, 3);
		assertThrows(IllegalArgumentException.class, () -> lcgHashFunction.apply(new int[0]));
	}

	@Test
	public void invalidApply3InputArray() {
		final LCGHashFunction lcgHashFunction = new LCGHashFunction(1, 2, 3);
		assertThrows(IllegalArgumentException.class, () -> lcgHashFunction.apply(new int[0], new int[4], 4));
	}

	@Test
	public void invalidApply3ValidInputArrayAndLength() {
		final LCGHashFunction lcgHashFunction = new LCGHashFunction(1, 2, 3);
		assertThrows(IllegalArgumentException.class, () -> lcgHashFunction.apply(new int[3], new int[4], 4));
	}

	@Test
	public void invalidApply3ValidInputOutputArray() {
		final LCGHashFunction lcgHashFunction = new LCGHashFunction(1, 2, 3);
		assertThrows(IllegalArgumentException.class, () -> lcgHashFunction.apply(new int[4], new int[3], 4));
	}

	@Test
	public void sameValues() {

		final int p = 978455454;
		final long a = 361681098;
		final long b = 189455706;

		final LCGHashFunction lcgHashFunction = new LCGHashFunction(p, a, b);

		assertEquals(p, lcgHashFunction.getP());
		assertEquals(a, lcgHashFunction.getA());
		assertEquals(b, lcgHashFunction.getB());
	}

	@Test
	public void equality() {

		final int p = 978455454;
		final long a = 361681098;
		final long b = 189455706;

		final LCGHashFunction lcgHashFunction = new LCGHashFunction(p, a, b);

		assertEquals(lcgHashFunction, new LCGHashFunction(p, a, b));
		assertNotEquals(lcgHashFunction, new LCGHashFunction(p + 1, a, b));
		assertNotEquals(lcgHashFunction, new LCGHashFunction(p, a + 1, b));
		assertNotEquals(lcgHashFunction, new LCGHashFunction(p, a, b + 1));
		assertNotEquals(lcgHashFunction, null);
	}

	@Test
	public void hashEquality() {

		final int p = 978455454;
		final long a = 361681098;
		final long b = 189455706;

		final LCGHashFunction lcgHashFunction = new LCGHashFunction(p, a, b);

		final Random random = new Random();
		final int arraySize = 50;
		final int[] array = new int[arraySize];
		for (int i = 0; i < arraySize; i++) {
			array[i] = random.nextInt();
		}

		assertEquals(lcgHashFunction.apply(array[arraySize - 2]),
				new LCGHashFunction(p, a, b).apply(array[arraySize - 2]));
		assertArrayEquals(lcgHashFunction.apply(array), new LCGHashFunction(p, a, b).apply(array));

		int[] out1 = new int[arraySize];
		int[] out2 = new int[arraySize];

		lcgHashFunction.apply(array, out1, arraySize - 2);
		new LCGHashFunction(p, a, b).apply(array, out2, arraySize - 2);
		for (int i = 0; i < arraySize - 2; i++) {
			assertEquals(out1[i], out2[i]);
		}
	}

	@Test
	public void knownOutput() {
		final int p = Integer.MAX_VALUE;
		final long a = 361681098;
		final long b = 189455706;

		final int[] expectedValues = { 189455706, 551136804, 912817902, 1274499000, 1636180098, 1997861196, 212058647,
				573739745, 935420843, 1297101941, 1658783039, 2020464137, 234661588, 596342686, 958023784, 1319704882,
				1681385980, 2043067078, 257264529, 618945627 };

		final LCGHashFunction lcgHashFunction = new LCGHashFunction(p, a, b);

		/**
		 * Using ::apply(int)
		 */
		for (int i = 0; i < expectedValues.length; i++) {
			final Integer hashedValue = lcgHashFunction.apply(i);

			assertNotNull(hashedValue);
			assertEquals(expectedValues[i], (int) hashedValue, "Invalid entry " + i);
		}

		/**
		 * Using ::apply(int[])
		 */
		final int[] input = new int[expectedValues.length];
		for (int i = 0; i < expectedValues.length; i++) {
			input[i] = i;
		}

		final int[] hashedValue = lcgHashFunction.apply(input);
		assertNotNull(hashedValue);
		assertArrayEquals(expectedValues, hashedValue);
	}
}