package net.bmahe.lsh.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

public class JaccardMeasureTest {

	final static double EPSILON = 0.001;

	@Test
	public void similarityNoDoc1() {
		assertThrows(NullPointerException.class, () -> JaccardMeasure.jaccardSimilarity(null, new HashSet<>()));
	}

	@Test
	public void similarityNoDoc2() {
		assertThrows(NullPointerException.class, () -> JaccardMeasure.jaccardSimilarity(new HashSet<>(), null));
	}

	@Test
	public void similarityBasic() {

		final Set<Integer> document1 = Set.of(1, 2, 3, 4);
		final Set<Integer> document2 = Set.of(3, 4, 5, 6);
		final Set<Integer> document3 = Set.of(5, 6, 7, 8);
		final Set<Integer> document4 = Set.of(5, 6, 2, 3, 4);

		assertEquals(0.0, JaccardMeasure.jaccardSimilarity(Collections.emptySet(), Collections.emptySet()), EPSILON);
		assertEquals(1.0, JaccardMeasure.jaccardDistance(Collections.emptySet(), Collections.emptySet()), EPSILON);

		assertEquals(1.0, JaccardMeasure.jaccardSimilarity(document1, document1), EPSILON);
		assertEquals(0.0, JaccardMeasure.jaccardDistance(document1, document1), EPSILON);

		assertEquals(0.3333, JaccardMeasure.jaccardSimilarity(document1, document2), EPSILON);
		assertEquals(0.6666, JaccardMeasure.jaccardDistance(document1, document2), EPSILON);

		assertEquals(0.3333, JaccardMeasure.jaccardSimilarity(document2, document1), EPSILON);
		assertEquals(0.6666, JaccardMeasure.jaccardDistance(document2, document1), EPSILON);

		assertEquals(0.0, JaccardMeasure.jaccardSimilarity(document1, document3), EPSILON);
		assertEquals(1.0, JaccardMeasure.jaccardDistance(document1, document3), EPSILON);

		assertEquals(0.5, JaccardMeasure.jaccardSimilarity(document1, document4), EPSILON);
		assertEquals(0.5, JaccardMeasure.jaccardDistance(document1, document4), EPSILON);
	}

	@Test
	public void similarityHashNoHash1() {
		assertThrows(NullPointerException.class, () -> JaccardMeasure.jaccardSimilarityHash(null, new int[2]));
	}

	@Test
	public void similarityHashNoHash2() {
		assertThrows(NullPointerException.class, () -> JaccardMeasure.jaccardSimilarityHash(new int[2], null));
	}

	@Test
	public void distanceHashDifferentLength() {
		assertThrows(IllegalArgumentException.class, () -> JaccardMeasure.jaccardDistanceHash(new int[2], new int[5]));
	}

	@Test
	public void distanceHashNoHash1() {
		assertThrows(NullPointerException.class, () -> JaccardMeasure.jaccardDistanceHash(null, new int[2]));
	}

	@Test
	public void distanceHashNoHash2() {
		assertThrows(NullPointerException.class, () -> JaccardMeasure.jaccardDistanceHash(new int[2], null));
	}

	@Test
	public void similarityHashDifferentLength() {
		assertThrows(IllegalArgumentException.class,
				() -> JaccardMeasure.jaccardSimilarityHash(new int[2], new int[5]));
	}

	@Test
	public void similarityHash() {
		assertEquals(0.0, JaccardMeasure.jaccardSimilarityHash(new int[] {}, new int[] {}), EPSILON);
		assertEquals(0.0, JaccardMeasure.jaccardSimilarityHash(new int[] { 1 }, new int[] { 3 }), EPSILON);
		assertEquals(0.0, JaccardMeasure.jaccardSimilarityHash(new int[] { 1, 4 }, new int[] { 3, 7 }), EPSILON);
		assertEquals(0.5, JaccardMeasure.jaccardSimilarityHash(new int[] { 1, 4 }, new int[] { 3, 4 }), EPSILON);
		assertEquals(1.0, JaccardMeasure.jaccardSimilarityHash(new int[] { 3, 4 }, new int[] { 3, 4 }), EPSILON);
	}

	@Test
	public void distanceHash() {
		assertEquals(1.0, JaccardMeasure.jaccardDistanceHash(new int[] { 1 }, new int[] { 3 }), EPSILON);
		assertEquals(1.0, JaccardMeasure.jaccardDistanceHash(new int[] { 1, 4 }, new int[] { 3, 7 }), EPSILON);
		assertEquals(0.5, JaccardMeasure.jaccardDistanceHash(new int[] { 1, 4 }, new int[] { 3, 4 }), EPSILON);
		assertEquals(0.0, JaccardMeasure.jaccardDistanceHash(new int[] { 3, 4 }, new int[] { 3, 4 }), EPSILON);
	}

}