package net.bmahe.lsh.core.hashfunctions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Random;

import org.junit.jupiter.api.Test;

public class LCGGroupHashFunctionsFactoryTest {

	@Test
	public void noLcgGenerator() {
		assertThrows(NullPointerException.class, () -> new LCGGroupHashFunctionsFactory(null));
	}

	@Test
	public void invalidHashSize() {

		final Random random = new Random();
		final LCGHashGenerator hashGenerator = new LCGHashGenerator(random);

		final LCGGroupHashFunctionsFactory hashFunctionsFactory = new LCGGroupHashFunctionsFactory(hashGenerator);

		assertThrows(IllegalArgumentException.class, () -> hashFunctionsFactory.build(0));
	}

	@Test
	public void validHashSize() {

		final Random random = new Random();
		final LCGHashGenerator hashGenerator = new LCGHashGenerator(random);

		final LCGGroupHashFunctionsFactory hashFunctionsFactory = new LCGGroupHashFunctionsFactory(hashGenerator);

		final int hashSize = 20;
		final GroupHashFunctions groupHashFunctions = hashFunctionsFactory.build(hashSize);
		assertEquals(groupHashFunctions.getNumHashFunctions(), hashSize);
	}
}