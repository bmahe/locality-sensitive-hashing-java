package net.bmahe.lsh.core.hashfunctions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Random;

import org.junit.jupiter.api.Test;

public class LCGGroupHashFunctionsTest {

	@Test
	public void invalidHashSize() {
		final Random random = new Random();
		final LCGHashGenerator lcgHashGenerator = new LCGHashGenerator(random);

		assertThrows(IllegalArgumentException.class, () -> new LCGGroupHashFunctions(0, lcgHashGenerator));
	}

	@Test
	public void negativehashIdGet() {
		final Random random = new Random();
		final LCGHashGenerator lcgHashGenerator = new LCGHashGenerator(random);

		final LCGGroupHashFunctions groupHashFunctions = new LCGGroupHashFunctions(10, lcgHashGenerator);
		assertThrows(IllegalArgumentException.class, () -> groupHashFunctions.getHashFunction(-1));
	}

	@Test
	public void outOfRangehashIdGet() {
		final Random random = new Random();
		final LCGHashGenerator lcgHashGenerator = new LCGHashGenerator(random);

		final LCGGroupHashFunctions groupHashFunctions = new LCGGroupHashFunctions(10, lcgHashGenerator);
		assertThrows(IllegalArgumentException.class, () -> groupHashFunctions.getHashFunction(10));
	}

	@Test
	public void negativehashIdApply() {
		final Random random = new Random();
		final LCGHashGenerator lcgHashGenerator = new LCGHashGenerator(random);

		final LCGGroupHashFunctions groupHashFunctions = new LCGGroupHashFunctions(10, lcgHashGenerator);
		assertThrows(IllegalArgumentException.class, () -> groupHashFunctions.apply(-1, 1));
	}

	@Test
	public void outOfRangehashIdApply() {
		final Random random = new Random();
		final LCGHashGenerator lcgHashGenerator = new LCGHashGenerator(random);

		final LCGGroupHashFunctions groupHashFunctions = new LCGGroupHashFunctions(10, lcgHashGenerator);
		assertThrows(IllegalArgumentException.class, () -> groupHashFunctions.apply(10, 1));
	}

	@Test
	public void noHashGenerator() {
		assertThrows(NullPointerException.class, () -> new LCGGroupHashFunctions(10, null));
	}

	@Test
	public void sameHashSize() {
		final Random random = new Random();
		final LCGHashGenerator lcgHashGenerator = new LCGHashGenerator(random);

		final int hashSize = 20;
		final LCGGroupHashFunctions groupHashFunctions = new LCGGroupHashFunctions(hashSize, lcgHashGenerator);
		assertEquals(hashSize, groupHashFunctions.getNumHashFunctions());
	}

	@Test
	public void reproducible() {
		final long seed = 68456421321L;
		final int hashSize = 20;

		final Random random1 = new Random(seed);
		final LCGHashGenerator lcgHashGenerator1 = new LCGHashGenerator(random1);
		final LCGGroupHashFunctions groupHashFunctions1 = new LCGGroupHashFunctions(hashSize, lcgHashGenerator1);
		assertEquals(hashSize, groupHashFunctions1.getNumHashFunctions());

		final Random random2 = new Random(seed);
		final LCGHashGenerator lcgHashGenerator2 = new LCGHashGenerator(random2);
		final LCGGroupHashFunctions groupHashFunctions2 = new LCGGroupHashFunctions(hashSize, lcgHashGenerator2);
		assertEquals(hashSize, groupHashFunctions2.getNumHashFunctions());

		for (int hashFunctionId = 0; hashFunctionId < hashSize; hashFunctionId++) {
			assertNotNull(groupHashFunctions1.getHashFunction(hashFunctionId));
			assertNotNull(groupHashFunctions2.getHashFunction(hashFunctionId));

			assertEquals(groupHashFunctions1.getHashFunction(hashFunctionId),
					groupHashFunctions2.getHashFunction(hashFunctionId));

			for (int j = 0; j < 100; j++) {
				assertEquals(groupHashFunctions1.apply(hashFunctionId, j),
						groupHashFunctions2.apply(hashFunctionId, j));
			}
		}
	}

	@Test
	public void verifyHashFunctionsCoefficients() {

		final int hashSize = 20;
		final int p = 20;

		final long a1 = 2;
		final long b1 = 1;

		final long a2 = 4;
		final long b2 = 5;

		final long a3 = 6;
		final long b3 = 7;

		final LCGHashGenerator lcgHashGenerator = mock(LCGHashGenerator.class);
		when(lcgHashGenerator.generate()).thenReturn(new LCGHashFunction(p, a1, b1))
				.thenReturn(new LCGHashFunction(p, a2, b2)).thenReturn(new LCGHashFunction(p, a3, b3));
		when(lcgHashGenerator.getP()).thenReturn(p);

		final LCGGroupHashFunctions groupHashFunctions = new LCGGroupHashFunctions(hashSize, lcgHashGenerator);
		assertEquals(hashSize, groupHashFunctions.getNumHashFunctions());
		assertEquals(hashSize, groupHashFunctions.getNumHashFunctions());

		assertEquals(a1, groupHashFunctions.getHashFunction(0).getA());
		assertEquals(b1, groupHashFunctions.getHashFunction(0).getB());
		assertEquals(p, groupHashFunctions.getHashFunction(0).getP());

		assertEquals(a2, groupHashFunctions.getHashFunction(1).getA());
		assertEquals(b2, groupHashFunctions.getHashFunction(1).getB());
		assertEquals(p, groupHashFunctions.getHashFunction(1).getP());

		for (int i = 2; i < hashSize; i++) {
			assertEquals(a3, groupHashFunctions.getHashFunction(i).getA());
			assertEquals(b3, groupHashFunctions.getHashFunction(i).getB());
			assertEquals(p, groupHashFunctions.getHashFunction(i).getP());
		}
	}

	@Test
	public void verifyHashingComputation() {

		final int hashSize = 20;
		final int p = 20;
		final long a = 2;
		final long b = 1;

		final LCGHashGenerator lcgHashGenerator = mock(LCGHashGenerator.class);
		when(lcgHashGenerator.generate()).thenReturn(new LCGHashFunction(p, a, b));
		when(lcgHashGenerator.getP()).thenReturn(p);

		final LCGGroupHashFunctions groupHashFunctions = new LCGGroupHashFunctions(hashSize, lcgHashGenerator);

		for (int hashFunctionIdx = 0; hashFunctionIdx < hashSize; hashFunctionIdx++) {
			for (int i = 0; i < 100; i++) {
				final int expectedValue = (int) ((a * i + b) % p);
				assertEquals(expectedValue, groupHashFunctions.apply(hashFunctionIdx, i));
			}
		}

		// A few manual spot checks
		assertEquals(1, groupHashFunctions.apply(2, 0));
		assertEquals(3, groupHashFunctions.apply(3, 1));
		assertEquals(1, groupHashFunctions.apply(5, 10));
	}
}