package net.bmahe.lsh.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

public class CosineMeasureTest {

	private final static double EPSILON = 0.01;

	@Test
	public void noHash1() {
		assertThrows(NullPointerException.class, () -> CosineMeasure.cosineSimilarityHash(null, new int[] { 2 }));
	}

	@Test
	public void noHash2() {
		assertThrows(NullPointerException.class, () -> CosineMeasure.cosineSimilarityHash(new int[] { 2 }, null));
	}

	@Test
	public void hashMustHaveSameLength() {
		assertThrows(IllegalArgumentException.class,
				() -> CosineMeasure.cosineSimilarityHash(new int[] { 3, 5 }, new int[] { 2 }));
	}

	@Test
	public void hashEmptyReturnZero() {
		assertEquals(0.0, CosineMeasure.cosineSimilarityHash(new int[] {}, new int[] {}), 0.01);
	}

	@Test
	public void hashKnownValues() {

		/**
		 * No matching values => 0 values agrees => cos(pi)
		 */
		assertEquals(-1.0, CosineMeasure.cosineSimilarityHash(new int[] { 1, 2, 3, 4, 5 }, new int[] { 2, 3, 4, 5, 6 }),
				EPSILON);

		/**
		 * All values agree => cos(0)
		 */
		assertEquals(1.0, CosineMeasure.cosineSimilarityHash(new int[] { 1, 2, 3, 4, 5 }, new int[] { 1, 2, 3, 4, 5 }),
				EPSILON);

		/**
		 * Half values agree => cos(pi/2)
		 */
		assertEquals(0.0,
				CosineMeasure.cosineSimilarityHash(new int[] { 1, 2, 3, 4, 5, 6 }, new int[] { -1, -2, -3, 4, 5, 6 }),
				EPSILON);

	}
}