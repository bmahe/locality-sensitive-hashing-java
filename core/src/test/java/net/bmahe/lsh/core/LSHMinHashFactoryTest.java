package net.bmahe.lsh.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

public class LSHMinHashFactoryTest {

	@Test
	public void computeBandsNoZeroRows() {
		assertThrows(IllegalArgumentException.class, () -> LSHMinHashFactory.computeNumBands(0, 0.5));
	}

	@Test
	public void computeBandsNoNegativeThreshold() {
		assertThrows(IllegalArgumentException.class, () -> LSHMinHashFactory.computeNumBands(1, -0.5));
	}

	@Test
	public void computeBandsNoBigThreshold() {
		assertThrows(IllegalArgumentException.class, () -> LSHMinHashFactory.computeNumBands(1, 50.5));
	}

	@Test
	public void computeBands() {
		/**
		 * Based on some precomputed values
		 */
		assertEquals(11, LSHMinHashFactory.computeNumBands(3, 0.46));
		assertEquals(15, LSHMinHashFactory.computeNumBands(4, 0.51));
		assertEquals(166, LSHMinHashFactory.computeNumBands(10, 0.60));
		assertEquals(2, LSHMinHashFactory.computeNumBands(15, 0.96));
	}
}