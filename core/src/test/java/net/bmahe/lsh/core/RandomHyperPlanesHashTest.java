package net.bmahe.lsh.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Objects;
import java.util.Random;

import org.apache.commons.lang3.Validate;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;
import org.junit.jupiter.api.Test;

public class RandomHyperPlanesHashTest {

	@Test
	public void noNegativeInputSize() {
		assertThrows(IllegalArgumentException.class, () -> new RandomHyperPlanesHash(-10, 10, new Random()));
	}

	@Test
	public void noNegativeHashSize() {
		assertThrows(IllegalArgumentException.class, () -> new RandomHyperPlanesHash(10, -10, new Random()));
	}

	@Test
	public void noNullRandom() {
		assertThrows(NullPointerException.class, () -> new RandomHyperPlanesHash(10, 10, null));
	}

	@Test
	public void validateInit() {
		final Random random = new Random();

		final int inputSize = 10;
		final int hashSize = 101;
		final RandomHyperPlanesHash randomHyperPlanesHash = new RandomHyperPlanesHash(inputSize, hashSize, random);

		assertEquals(inputSize, randomHyperPlanesHash.getInputSize());
		assertEquals(hashSize, randomHyperPlanesHash.getHashSize());
		assertEquals(random, randomHyperPlanesHash.getRandom());

		final double[][] hyperPlaneVectors = randomHyperPlanesHash.getHyperPlaneVectors();
		assertEquals(hashSize, hyperPlaneVectors.length);
		for (int i = 0; i < hashSize; i++) {
			assertEquals(inputSize, hyperPlaneVectors[i].length);

			/**
			 * Each value should be either 1.0 or -1.0
			 */
			for (int j = 0; j < inputSize; j++) {
				assertEquals(1.0, Math.abs(hyperPlaneVectors[i][j]), 0.0001);
			}
		}
	}

	private RealVector toRealVector(final int[] vector) {
		Objects.requireNonNull(vector);
		Validate.isTrue(vector.length > 0);

		final ArrayRealVector realVector = new ArrayRealVector(vector.length);
		for (int i = 0; i < vector.length; i++) {
			int j = vector[i];
			realVector.addToEntry(i, (double) j);
		}

		return realVector;
	}

	private void compareHash(final int[] hash1, final int[] hash2, final double expectedCos) {
		final double estimatedCos = CosineMeasure.cosineSimilarityHash(hash1, hash2);
		System.out.println("Estimated cos: " + estimatedCos);

		final RealVector realVector = toRealVector(hash1);
		final RealVector realVector2 = toRealVector(hash2);

		final double cosine = realVector.cosine(realVector2);
		System.out.println("REAL COS: " + cosine);
		System.out.println();

		assertEquals(expectedCos, estimatedCos, 0.1);

	}

	@Test
	public void knownValues() {
		final Random random = new Random();

		final int inputSize = 2;
		final int hashSize = 1000;
		final RandomHyperPlanesHash randomHyperPlanesHash = new RandomHyperPlanesHash(inputSize, hashSize, random);

		final int[][] vector1 = { { -10, 0 }, { 5, 0 }, { 10, 0 }, { 0, 10 } };
		final int[][] vector2 = { { 10, 0 }, { 15, 0 }, { -10, 0 }, { 0, -10 } };
		final double[] expectedCosValue = { -1, 1, -1, -1 };

		for (int caseIdx = 0; caseIdx < expectedCosValue.length; caseIdx++) {
			final int[] vector1Int = vector1[caseIdx];
			final int[] vector2Int = vector2[caseIdx];

			assertEquals(vector1Int.length, vector2Int.length);

			final int[] hash1Int = randomHyperPlanesHash.hash(vector1Int);
			final int[] hash2Int = randomHyperPlanesHash.hash(vector2Int);

			compareHash(hash1Int, hash2Int, expectedCosValue[caseIdx]);

			final double[] vector1Double = new double[vector1Int.length];
			final double[] vector2Double = new double[vector1Int.length];
			for (int i = 0; i < vector1Int.length; i++) {
				vector1Double[i] = vector1Int[i];
				vector2Double[i] = vector2Int[i];
			}

			final int[] hash1Double = randomHyperPlanesHash.hash(vector1Double);
			final int[] hash2Double = randomHyperPlanesHash.hash(vector2Double);

			compareHash(hash1Double, hash2Double, expectedCosValue[caseIdx]);

		}
	}
}