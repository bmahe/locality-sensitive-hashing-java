package net.bmahe.lsh.core.hashfunctions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.junit.jupiter.api.Test;

public class LCGHashGeneratorTest {

	@Test
	public void randomIsRequired() {
		assertThrows(NullPointerException.class, () -> new LCGHashGenerator(null));
	}

	@Test
	public void checkP() {
		final Random random = new Random();

		final LCGHashGenerator hashGenerator = new LCGHashGenerator(random);
		assertNotNull(hashGenerator);

		assertEquals(LCGHashGenerator.DEFAULT_P, hashGenerator.getP());
	}

	@Test
	public void aCantBeZero() {
		final Random random = mock(Random.class);
		when(random.nextInt()).thenReturn(0);

		final LCGHashGenerator hashGenerator = new LCGHashGenerator(random);
		assertNotNull(hashGenerator);

		final LCGHashFunction hashFunction = hashGenerator.generate();

		assertNotEquals(0, hashFunction.getA());
	}

	@Test
	public void bCantBeZero() {
		final Random random = mock(Random.class);
		when(random.nextInt()).thenReturn(5).thenReturn(0);

		final LCGHashGenerator hashGenerator = new LCGHashGenerator(random);
		assertNotNull(hashGenerator);

		final LCGHashFunction hashFunction = hashGenerator.generate();

		assertNotEquals(0, hashFunction.getA());
		assertNotEquals(0, hashFunction.getB());
	}

	@Test
	public void spotCheckHashedValues() {
		final Random random = new Random();

		final LCGHashGenerator hashGenerator = new LCGHashGenerator(random);
		assertNotNull(hashGenerator);

		for (int i = 0; i < 100; i++) {
			final HashFunction hashFunction = hashGenerator.generate();
			assertNotNull(hashFunction);

			final Map<Integer, Integer> valueCount = new HashMap<>();
			for (int j = 0; j < 100; j++) {
				final Integer hashedValue = hashFunction.apply(j);

				assertTrue(hashedValue >= 0);
				assertTrue(hashedValue < hashGenerator.getP());

				valueCount.put(hashedValue, valueCount.getOrDefault(valueCount, 0) + 1);
			}

			// Something is really wrong if we always get the same value
			assertTrue(valueCount.size() > 1);
		}
	}

	@Test
	public void reproducibleResults() {
		final long seed = 894556465465L;

		final Random random1 = new Random(seed);
		final LCGHashGenerator hashGenerator1 = new LCGHashGenerator(random1);
		assertNotNull(hashGenerator1);

		final Random random2 = new Random(seed);
		final LCGHashGenerator hashGenerator2 = new LCGHashGenerator(random2);
		assertNotNull(hashGenerator2);

		assertNotNull(hashGenerator1.getRandom());
		assertNotNull(hashGenerator2.getRandom());

		for (int i = 0; i < 100; i++) {
			final HashFunction hashFunction1 = hashGenerator1.generate();
			assertNotNull(hashFunction1);

			final HashFunction hashFunction2 = hashGenerator2.generate();
			assertNotNull(hashFunction2);

			assertEquals(hashFunction1, hashFunction2);
		}
	}
}