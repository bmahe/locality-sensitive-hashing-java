package net.bmahe.lsh.core;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.AdditionalAnswers.returnsLastArg;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import net.bmahe.lsh.core.hashfunctions.GroupHashFunctions;
import net.bmahe.lsh.core.hashfunctions.GroupHashFunctionsFactory;

public class LSHTest {

	@Test
	public void hash2BucketsNoHash() {

		final GroupHashFunctionsFactory grouphashFunctionsFactory = mock(GroupHashFunctionsFactory.class);
		final LocalSensitiveFunction localSensitiveFunction = mock(LocalSensitiveFunction.class);

		final int numBands = 2;
		final int numBuckets = 3;

		final LSH lsh = new LSH(grouphashFunctionsFactory, localSensitiveFunction, numBands, numBuckets);

		assertThrows(NullPointerException.class, () -> lsh.hash2Buckets(null));
	}

	@Test
	public void hash2BucketsZeroHash() {

		final GroupHashFunctionsFactory grouphashFunctionsFactory = mock(GroupHashFunctionsFactory.class);
		final LocalSensitiveFunction localSensitiveFunction = mock(LocalSensitiveFunction.class);

		final int numBands = 2;
		final int numBuckets = 3;

		final LSH lsh = new LSH(grouphashFunctionsFactory, localSensitiveFunction, numBands, numBuckets);

		assertThrows(IllegalArgumentException.class, () -> lsh.hash2Buckets(new int[0]));
	}

	@Test
	public void hash2BucketsSimpleIdentity() {

		final GroupHashFunctionsFactory mockGrouphashFunctionsFactory = mock(GroupHashFunctionsFactory.class);
		final LocalSensitiveFunction mockLocalSensitiveFunction = mock(LocalSensitiveFunction.class);
		final GroupHashFunctions mockGroupHashFunctions = mock(GroupHashFunctions.class);

		when(mockGrouphashFunctionsFactory.build(anyInt())).thenReturn(mockGroupHashFunctions);
		when(mockGroupHashFunctions.apply(anyInt(), anyInt())).thenAnswer(returnsLastArg());

		final int[] inputHash = { 5, 4, 3, 10, 88 };

		/*
		 * This will ensure that a single element of the input will be used for each
		 * output element
		 */
		final int numBands = inputHash.length;

		/**
		 * This will make sure no element will loop through the modulo
		 */
		final int numBuckets = 1 + Arrays.stream(inputHash).max().orElseThrow();

		final LSH lsh = new LSH(mockGrouphashFunctionsFactory, mockLocalSensitiveFunction, numBands, numBuckets);

		assertEquals(numBands, lsh.getNumBands());
		assertEquals(numBuckets, lsh.getNumBuckets());

		final int[] buckets = lsh.hash2Buckets(inputHash);
		assertNotNull(buckets);
		assertEquals(numBands, buckets.length);
		assertEquals(inputHash.length, buckets.length);
		assertArrayEquals(inputHash, buckets);
	}

}