package net.bmahe.lsh.core;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.Random;

import org.junit.jupiter.api.Test;

import net.bmahe.lsh.core.hashfunctions.GroupHashFunctionsFactory;
import net.bmahe.lsh.core.hashfunctions.LCGGroupHashFunctionsFactory;
import net.bmahe.lsh.core.hashfunctions.LCGHashGenerator;

public class LSHMinHashTest {

	@Test
	public void simple() {

		final Random random = new Random();
		final LCGHashGenerator hashGenerator = new LCGHashGenerator(random);

		final int hashSize = 10;
		final GroupHashFunctionsFactory groupHashFunctionsFactory = new LCGGroupHashFunctionsFactory(hashGenerator);
		final MinHash minHash = new MinHash(hashSize, groupHashFunctionsFactory);

		final LSH lshMinHash = new LSH(groupHashFunctionsFactory, minHash, 5, 10);

		final int[] document = { 45648, 12315, 546465465, 123 };
		final int[] hash = minHash.hash(document);
		final int[] lsh = lshMinHash.hash(document);

		System.out.println("Hashing : " + document);
		System.out.println("Computed hash: " + Arrays.deepToString(Arrays.asList(hash).toArray()));
		System.out.println("Computed lsh: " + Arrays.deepToString(Arrays.asList(lsh).toArray()));

		final int[] document2 = { 456, 1, 6, 49687, 3 };
		final int[] hash2 = minHash.hash(document2);
		final int[] lsh2 = lshMinHash.hash(document2);

		System.out.println("Hashing : " + document2);
		System.out.println("Computed hash: " + Arrays.deepToString(Arrays.asList(hash2).toArray()));
		System.out.println("Computed lsh: " + Arrays.deepToString(Arrays.asList(lsh2).toArray()));

	}

	@Test
	public void reproducible() {
		final long seed = 9029049432902393L;

		final int numBands = 4;
		final int numBuckets = 100;

		final LSH lshMinHash1 = LSHMinHashFactory.build(numBands, numBuckets, seed);
		final LSH lshMinHash2 = LSHMinHashFactory.build(numBands, numBuckets, seed);

		assertEquals(numBands, lshMinHash1.getNumBands());
		assertEquals(numBands, lshMinHash2.getNumBands());

		assertEquals(numBuckets, lshMinHash1.getNumBuckets());
		assertEquals(numBuckets, lshMinHash2.getNumBuckets());

		final int[] document = { 4564, 12312, 811229, 0, 556 };
		assertArrayEquals(lshMinHash1.hash(document), lshMinHash2.hash(document));
	}
}