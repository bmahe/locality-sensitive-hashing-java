package net.bmahe.lsh.core;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.Set;

import org.apache.commons.lang3.Validate;
import org.junit.jupiter.api.Test;

import net.bmahe.lsh.core.hashfunctions.GroupHashFunctionsFactory;
import net.bmahe.lsh.core.hashfunctions.LCGGroupHashFunctionsFactory;
import net.bmahe.lsh.core.hashfunctions.LCGHashGenerator;

public class MinHashTest {

	class SparseVector {
		public int[] indices;
		public int[] values;

		public SparseVector(final int size) {
			indices = new int[size];
			values = new int[size];
		}
	}

	private SparseVector doc2SparseVector(Map<String, Integer> dictionnary, Set<String> document) {
		Objects.requireNonNull(dictionnary);
		Objects.requireNonNull(document);
		Validate.isTrue(document.size() > 0);

		final SparseVector sparseVector = new SparseVector(document.size());
		int i = 0;
		for (String word : document) {
			sparseVector.indices[i] = dictionnary.get(word);
			sparseVector.values[i] = dictionnary.get(word);
			i++;
		}

		return sparseVector;
	}

	private Map<String, Integer> buildDictionnary(Set<String> documents) {
		Objects.requireNonNull(documents);
		Validate.isTrue(documents.size() > 0);

		final Map<String, Integer> dictionnary = new HashMap<>();
		for (final String document : documents) {
			dictionnary.computeIfAbsent(document, k -> dictionnary.size());
		}

		return dictionnary;
	}

	@Test
	public void noHashSize() {
		final Random random = new Random();
		final LCGHashGenerator hashGenerator = new LCGHashGenerator(random);

		final GroupHashFunctionsFactory groupHashFunctionsFactory = new LCGGroupHashFunctionsFactory(hashGenerator);
		assertThrows(IllegalArgumentException.class, () -> new MinHash(0, groupHashFunctionsFactory));

	}

	@Test
	public void noNegativeHashSize() {
		final Random random = new Random();
		final LCGHashGenerator hashGenerator = new LCGHashGenerator(random);

		final GroupHashFunctionsFactory groupHashFunctionsFactory = new LCGGroupHashFunctionsFactory(hashGenerator);
		assertThrows(IllegalArgumentException.class, () -> new MinHash(-1, groupHashFunctionsFactory));
		;
	}

	@Test
	public void noGroupHashDUnctionsFactory() {

		assertThrows(NullPointerException.class, () -> new MinHash(10, null));
	}

	@Test
	public void noDoubleHashSupported() {
		final Random random = new Random();
		final LCGHashGenerator hashGenerator = new LCGHashGenerator(random);

		final GroupHashFunctionsFactory groupHashFunctionsFactory = new LCGGroupHashFunctionsFactory(hashGenerator);
		final MinHash minHash = new MinHash(10, groupHashFunctionsFactory);

		assertThrows(UnsupportedOperationException.class, () -> minHash.hash(new double[] { 10, 12 }));
	}

	@Test
	public void noSparseDoubleHashSupported() {
		final Random random = new Random();
		final LCGHashGenerator hashGenerator = new LCGHashGenerator(random);

		final GroupHashFunctionsFactory groupHashFunctionsFactory = new LCGGroupHashFunctionsFactory(hashGenerator);
		final MinHash minHash = new MinHash(10, groupHashFunctionsFactory);

		assertThrows(UnsupportedOperationException.class,
				() -> minHash.hashSparse(new int[] { 10, 12 }, new double[] { 4., 5. }));
	}

	@Test
	public void simple() {

		final List<String> words = Arrays.asList("Green is not the blue color".split("\\W+"));
		final Set<String> documents = new HashSet<>(words);
		final Map<String, Integer> dictionnary = new HashMap<>();
		for (final String document : documents) {
			dictionnary.computeIfAbsent(document, k -> dictionnary.size());
		}

		System.out.println("Words: " + words);
		System.out.println("Documents: " + documents);
		System.out.println("Dictionnary: " + dictionnary);

		final Random random = new Random();
		final LCGHashGenerator hashGenerator = new LCGHashGenerator(random);

		final int hashSize = 10;
		final GroupHashFunctionsFactory groupHashFunctionsFactory = new LCGGroupHashFunctionsFactory(hashGenerator);
		final MinHash minHash = new MinHash(hashSize, groupHashFunctionsFactory);

		final Set<String> document = Set.of(words.get(0), words.get(1), words.get(2));

		final SparseVector docVec = doc2SparseVector(dictionnary, document);
		final int[] hash = minHash.hashSparse(docVec.indices, docVec.values);

		System.out.println("Hashing : " + document);
		System.out.println("Computed hash: " + Arrays.deepToString(Arrays.asList(hash).toArray()));

		final Set<String> document2 = Set.of(words.get(3), words.get(1), words.get(2));
		final SparseVector docVec2 = doc2SparseVector(dictionnary, document2);

		final int[] hash2 = minHash.hashSparse(docVec2.indices, docVec2.values);

		System.out.println("Hashing : " + document2);
		System.out.println("Computed hash: " + Arrays.deepToString(Arrays.asList(hash2).toArray()));
	}

	@Test
	public void correctHashSize() {
		final Random random = new Random();
		final LCGHashGenerator hashGenerator = new LCGHashGenerator(random);

		final int hashSize = 10;
		final GroupHashFunctionsFactory groupHashFunctionsFactory = new LCGGroupHashFunctionsFactory(hashGenerator);
		final MinHash minHash = new MinHash(hashSize, groupHashFunctionsFactory);

		assertEquals(hashSize, minHash.getHashSize());
	}

	/**
	 * Provided the same seed and configuration, we should get the same hash for the
	 * same input
	 */
	@Test
	public void reproducibleHash() {
		final long seed = 54323034043L;
		final int hashSize = 10;

		final List<String> words = Arrays.asList("Green is not the blue color".split("\\W+"));
		final Set<String> documents = new HashSet<>(words);
		final Map<String, Integer> dictionnary = buildDictionnary(documents);

		final Set<String> document1 = Set.of(words.get(0), words.get(1), words.get(2));
		final SparseVector docVec1 = doc2SparseVector(dictionnary, document1);
		final Set<String> document2 = Set.of(words.get(3), words.get(1), words.get(2));
		final SparseVector docVec2 = doc2SparseVector(dictionnary, document2);

		final Random randomA = new Random(seed);
		final LCGHashGenerator hashGeneratorA = new LCGHashGenerator(randomA);
		final GroupHashFunctionsFactory groupHashFunctionsFactoryA = new LCGGroupHashFunctionsFactory(hashGeneratorA);
		final MinHash minHashA = new MinHash(hashSize, groupHashFunctionsFactoryA);

		final int[] hashA = minHashA.hashSparse(docVec1.indices, docVec1.values);
		final int[] hashA2 = minHashA.hashSparse(docVec2.indices, docVec2.values);

		final Random randomB = new Random(seed);
		final LCGHashGenerator hashGeneratorB = new LCGHashGenerator(randomB);
		final GroupHashFunctionsFactory groupHashFunctionsFactoryB = new LCGGroupHashFunctionsFactory(hashGeneratorB);
		final MinHash minHashB = new MinHash(hashSize, groupHashFunctionsFactoryB);

		final int[] hashB = minHashB.hashSparse(docVec1.indices, docVec1.values);
		final int[] hashB2 = minHashB.hashSparse(docVec2.indices, docVec2.values);

		assertArrayEquals(hashA, hashB);
		assertArrayEquals(hashA2, hashB2);
	}

	@Test
	public void hashSparseDifferentOrderSameHash() {

		final Random random = new Random();
		final LCGHashGenerator hashGenerator = new LCGHashGenerator(random);

		final int hashSize = 10;
		final GroupHashFunctionsFactory groupHashFunctionsFactory = new LCGGroupHashFunctionsFactory(hashGenerator);
		final MinHash minHash = new MinHash(hashSize, groupHashFunctionsFactory);

		final SparseVector docVec1 = new SparseVector(5);
		docVec1.indices = new int[] { 4, 5, 6, 7, 8 };
		docVec1.values = new int[] { 4, 5, 6, 7, 8 };

		final SparseVector docVec2 = new SparseVector(5);
		docVec2.indices = new int[] { 7, 8, 5, 4, 6 };
		docVec2.values = new int[] { 7, 8, 5, 4, 6 };

		final int[] hash1 = minHash.hashSparse(docVec1.indices, docVec1.values);
		final int[] hash2 = minHash.hashSparse(docVec2.indices, docVec2.values);
		assertArrayEquals(hash1, hash2);
	}

	@Test
	public void hashDifferentOrderSameHash() {

		final Random random = new Random();
		final LCGHashGenerator hashGenerator = new LCGHashGenerator(random);

		final int hashSize = 10;
		final GroupHashFunctionsFactory groupHashFunctionsFactory = new LCGGroupHashFunctionsFactory(hashGenerator);
		final MinHash minHash = new MinHash(hashSize, groupHashFunctionsFactory);

		final int docSize = 200;
		int[] document = new int[docSize];
		for (int i = 0; i < docSize; i++) {
			document[i] = random.nextInt();
		}
		final int[] hash = minHash.hash(document);

		for (int i = 0; i < 20; i++) {
			final List<Integer> docList = new ArrayList<>();
			for (int j : document) {
				docList.add(j);
			}

			Collections.shuffle(docList);

			final int[] shuffledDoc = new int[docSize];
			for (int k = 0; k < docList.size(); k++) {
				final Integer integer = docList.get(k);
				shuffledDoc[k] = integer.hashCode();
			}

			final int[] hash2 = minHash.hash(shuffledDoc);
			assertArrayEquals(hash, hash2);
		}
	}
}