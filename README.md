[![pipeline status](https://gitlab.com/bmahe/locality-sensitive-hashing-java/badges/master/pipeline.svg)](https://gitlab.com/bmahe/locality-sensitive-hashing-java/commits/master)

# Locality Sensitive Hashing - Java

Java implementation of some LSH algorithms:
* MinHash
* Random Hyperplanes

Includes some examples and benchmarks

See website https://bmahe.gitlab.io/locality-sensitive-hashing-java/ for more information, quickstart and examples.

# License

Everything is under Apache License Version 2.0

# Running examples

Running the indexer for images:

```bash
DATASET_LFW=/home/bruno/data/lfw-deepfunneled/
mvn exec:exec -Dexec.executable="java" -Dexec.args="-classpath %classpath net.bmahe.lsh.examples.ImageIndexerDJL ./index.json 0 ${DATASET_LFW}"
```

The arguments are in order:

- Index file to write with the filename of each picture and its associated embedding vector
- Number of images to process. 0 means no limit
- Base directory for the LFW dataset
