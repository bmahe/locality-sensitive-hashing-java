set terminal png size 640,480 enhanced 
set output '${minHashJaccardSimilarityPng}'

set style fill transparent solid 0.35 noborder
set grid front

set title 'Jaccard similarity VS MinHash approximation'
set xlabel 'Jaccard similarity'
set ylabel 'MinHash similarity'

set xrange [0:1]
set yrange [0:1]

set datafile separator ","

numHashes = ${minHashJaccardSimilarity.num_hashes}
maxRows = numHashes*2 +1

plot \
    for [col=2:maxRows:2] '${minHashJaccardSimilarityCsv}' using 1:(column(col)-column(col+1)):(column(col)+column(col+1)) with filledcurves notitle, \
    for [col=2:maxRows:2] '${minHashJaccardSimilarityCsv}' using 1:col with lines title columnhead
