set terminal png size 640,480 enhanced 
set output '${lshMinHashSCurveExamplePng}'

set grid

set title 'S-Curve for pair of documents'
set xlabel 'Jaccard similarity'
set ylabel 'Probability of becoming candidate'

set xrange [0:1]
set yrange [0:1]

set datafile separator ","

numBands = ${lshMinHashSCurveExample.num_bands}
maxRows = numBands + 1

plot for [col=2:maxRows] '${lshMinHashSCurveExampleCsv}' using 1:col with lines title columnhead
