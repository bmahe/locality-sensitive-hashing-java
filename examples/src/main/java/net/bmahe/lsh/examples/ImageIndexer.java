package net.bmahe.lsh.examples;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.Validate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import ai.djl.Application;
import ai.djl.Device;
import ai.djl.MalformedModelException;
import ai.djl.engine.Engine;
import ai.djl.inference.Predictor;
import ai.djl.modality.cv.Image;
import ai.djl.modality.cv.ImageFactory;
import ai.djl.modality.cv.util.NDImageUtils;
import ai.djl.ndarray.NDArray;
import ai.djl.ndarray.NDList;
import ai.djl.opencv.OpenCVImageFactory;
import ai.djl.repository.zoo.Criteria;
import ai.djl.repository.zoo.ModelLoader;
import ai.djl.repository.zoo.ModelNotFoundException;
import ai.djl.repository.zoo.ModelZoo;
import ai.djl.repository.zoo.ZooModel;
import ai.djl.training.util.ProgressBar;
import ai.djl.translate.Translator;
import ai.djl.translate.TranslatorContext;

public class ImageIndexer {
	final static public Logger logger = LogManager.getLogger(ImageIndexer.class);

	final static private int RESNET_IMAGE_WIDTH = 250;
	final static private int RESNET_IMAGE_HEIGHT = 250;

	public static void initDJL(final String engineName) {
		final Set<String> allEngines = Engine.getAllEngines();
		logger.info("All engines: {}", allEngines);

		final Engine engine = Engine.getEngine(engineName);

		final Device[] devices = engine.getDevices();
		if (devices != null) {
			logger.debug("Devices:");
			for (Device device : devices) {
				logger.debug("\tDevice: {}", device);
				logger.debug("\tType: {}; Id: {}; isGPU: {}", device.getDeviceType(), device.getDeviceId(), device.isGpu());
			}
		}

		final Collection<ModelZoo> modelZoos = ModelZoo.listModelZoo();
		for (ModelZoo modelZoo : modelZoos) {

			logger.trace("{}", modelZoo.getGroupId());
			for (ModelLoader modelLoader : modelZoo.getModelLoaders()) {
				logger.trace("\t{}", modelLoader);
			}
		}

	}

	public static ZooModel<Image, float[]> loadDJLModel(final String engineName)
			throws ModelNotFoundException, MalformedModelException, IOException {
		Validate.notBlank(engineName);

		// tag::load_resnet18[]
		final Criteria<Image, float[]> criteria = Criteria.builder()
				.setTypes(Image.class, float[].class)
				.optEngine(engineName)
				.optTranslator(new Translator<Image, float[]>() {
					@Override
					public NDList processInput(final TranslatorContext ctx, final Image input) throws Exception {
						final NDArray array = input.toNDArray(ctx.getNDManager(), Image.Flag.COLOR);
						return new NDList(NDImageUtils.toTensor(array));
					}

					@Override
					public float[] processOutput(final TranslatorContext ctx, final NDList list) throws Exception {
						return list.singletonOrThrow()
								.toFloatArray();
					}

				})
				.optApplication(Application.CV.IMAGE_CLASSIFICATION)
				.optProgress(new ProgressBar())
				.optOption("trainParam", "false")
				.build();

		final ZooModel<Image, float[]> model = criteria.loadModel();
		logger.debug("Model loaded: {}", model.getName());
		// end::load_resnet18[]

		return model;
	}

	public static List<Path> collectPictures(final Path root, final int numImagesProcessed) throws IOException {
		try (Stream<Path> paths = Files.find(root,
				1000,
				(path, attr) -> path.toString()
						.endsWith(".jpg") && attr.isRegularFile() && attr.size() > 10)) {

			Stream<Path> filteredPaths = paths;
			if (numImagesProcessed > 0) {
				filteredPaths = filteredPaths.limit(numImagesProcessed);
			}

			return filteredPaths.toList();
		}
	}

	public static void main(String[] args) {
		Validate.isTrue(args.length == 3, "Was expecting 3 arguments but got %d", args.length);

		final String destinationFilename = args[0];
		final int numImagesProcessed = Integer.parseInt(args[1]);
		final String datasetBaseDir = args[2];

		final Path rootPictures = Path.of(datasetBaseDir);

		final String engineName = "PyTorch";
		try {
			logger.info("Instantiating object mapper");
			final ObjectMapperUtils objectMapperUtils = new ObjectMapperUtils();
			final ObjectMapper objectMapper = objectMapperUtils.createObjectMapper();

			logger.info("Gathering pictures to process");
			final List<Path> picturePaths = collectPictures(rootPictures, numImagesProcessed);
			logger.info("Collected {} pictures", picturePaths.size());

			logger.info("Initialize DJL");
			initDJL(engineName);

			logger.info("Loading model");
			final ZooModel<Image, float[]> model = loadDJLModel(engineName);

			final ImageFactory imageFactory = OpenCVImageFactory.getInstance();

			logger.info("Starting processing of the pictures");
			final long startCompute = System.currentTimeMillis();
			final AtomicInteger pictureProcessCount = new AtomicInteger();
			final List<ImageVector> imageVectors = picturePaths.stream()
					.map(path -> {
						try {
							final int processedCount = pictureProcessCount.incrementAndGet();
							if (processedCount % 50 == 0) {
								logger.debug("{} pictures processed", processedCount);
							}

							final Image image = imageFactory.fromFile(path);

							try (final Predictor<Image, float[]> predictor = model.newPredictor()) {
								final Image resizedImage = image.resize(RESNET_IMAGE_WIDTH, RESNET_IMAGE_HEIGHT, false);
								float[] predictedEmbedding = predictor.predict(resizedImage);

								// Name of the person is the directory containing the picture
								final String label = path.getName(path.getNameCount() - 2)
										.toString();
								return new ImageVector(path.toString(), label, predictedEmbedding);
							}
						} catch (Exception e) {
							logger.warn("Skipping {}", path, e);
							return null;
						}
					})
					.filter(embedding -> embedding != null)
					.toList();
			final long endCompute = System.currentTimeMillis();
			logger.debug("Total computation time: {} ms", endCompute - startCompute);
			logger.debug("Average computation per path: {} ms", (endCompute - startCompute) / picturePaths.size());

			logger.info("Writing {} records to {}", imageVectors.size(), destinationFilename);
			final File destinationFile = new File(destinationFilename);
			objectMapper.writeValue(destinationFile, imageVectors);
			final long destinationFileSizeBytes = FileUtils.sizeOf(destinationFile);
			logger.debug("File size: {}", FileUtils.byteCountToDisplaySize(destinationFileSizeBytes));

		} catch (

		Exception e) {
			logger.error(e);
			System.exit(-1);
		}
	}
}
