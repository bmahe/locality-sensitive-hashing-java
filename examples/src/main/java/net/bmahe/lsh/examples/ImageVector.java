package net.bmahe.lsh.examples;

import java.util.Arrays;
import java.util.Objects;

import org.apache.commons.lang3.Validate;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ImageVector {
	private final String filename;
	private final String label;
	private final float[] vector;

	@JsonCreator
	public ImageVector(@JsonProperty("filename") final String _filename, @JsonProperty("label") final String _label,
			@JsonProperty("vector") final float[] _vector) {
		Validate.notBlank(_filename);
		Validate.notBlank(_label);
		Objects.requireNonNull(_vector);
		Validate.isTrue(_vector.length > 0);

		this.filename = _filename;
		this.label = _label;
		this.vector = _vector;
	}

	public String getFilename() {
		return filename;
	}

	public String getLabel() {
		return label;
	}

	public float[] getVector() {
		return vector;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((filename == null) ? 0 : filename.hashCode());
		result = prime * result + ((label == null) ? 0 : label.hashCode());
		result = prime * result + Arrays.hashCode(vector);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImageVector other = (ImageVector) obj;
		if (filename == null) {
			if (other.filename != null)
				return false;
		} else if (!filename.equals(other.filename))
			return false;
		if (label == null) {
			if (other.label != null)
				return false;
		} else if (!label.equals(other.label))
			return false;
		if (!Arrays.equals(vector, other.vector))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ImageVector [filename=" + filename + ", label=" + label + ", vector=" + Arrays.toString(vector) + "]";
	}
}