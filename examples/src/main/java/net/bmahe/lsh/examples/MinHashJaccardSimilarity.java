package net.bmahe.lsh.examples;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.bmahe.lsh.core.JaccardMeasure;
import net.bmahe.lsh.core.MinHash;
import net.bmahe.lsh.core.hashfunctions.GroupHashFunctionsFactory;
import net.bmahe.lsh.core.hashfunctions.LCGGroupHashFunctionsFactory;
import net.bmahe.lsh.core.hashfunctions.LCGHashGenerator;

public class MinHashJaccardSimilarity {
	final static public Logger logger = LogManager.getLogger(MinHashJaccardSimilarity.class);

	final static public int DEFAULT_DICTIONNARY_SIZE = 1000;
	final static public int DEFAULT_DOCUMENT_SIZE = 100;
	final static public int DEFAULT_NUM_DOCUMENTS = 100;

	final static private String PARAM_DEST_CSV_FILE = "d";
	final static private String PARAM_NUM_DOCUMENTS = "n";
	final static private String PARAM_DOCUMENT_SIZE = "s";

	/**
	 * Bins are used to build histograms for the s-curve graph.
	 * 
	 * 
	 * Number of bins is equal to ((max - min) / width) + 1
	 * 
	 * The extra one is due to when the similarity is 1.0
	 */
	final static private double BINS_WIDTH = 0.01;
	final static private int NUM_BINS = 1 + (int) (1. / BINS_WIDTH);

	final private Random random = new Random();

	private List<Integer> generateDictionnary() {
		return Stream.iterate(0, t -> t < DEFAULT_DICTIONNARY_SIZE, e -> e + 1).collect(Collectors.toList());
	}

	// tag::generate_document[]
	private Set<Integer> generateDocument(final List<Integer> dictionnary, final int size) {

		final Set<Integer> document = new HashSet<>();

		while (document.size() < size) { // <1>
			final int index = random.nextInt(dictionnary.size()); // <2>

			final Integer element = dictionnary.get(index);
			document.add(element);
		}

		return document;
	}
	// end::generate_document[]

	// tag::generate_document_variations[]
	private Collection<Set<Integer>> generateVariations(final Set<Integer> document) {
		final Set<Set<Integer>> variations = new HashSet<>();

		final int documentSize = document.size();

		int[] copyDocument = new int[documentSize];
		for (int j = 0; j < documentSize; j++) { // <1>
			final Iterator<Integer> documentIt = document.iterator();
			/**
			 * Re-init
			 */
			for (int k = 0; k < documentSize; k++) {
				copyDocument[k] = documentIt.next();
			}

			/**
			 * Remove up to j elements
			 */
			for (int k = 0; k < j; k++) {
				copyDocument[k] = -1; // <2>
			}

			/**
			 * Re-serialize
			 */
			final Set<Integer> newDoc = new HashSet<>();
			for (int k = 0; k < documentSize; k++) {
				if (copyDocument[k] != -1) {
					newDoc.add(copyDocument[k]);
				}
			}

			variations.add(newDoc); // <3>
		}

		return variations;
	}
	// end::generate_document_variations[]

	public static void cliError(final Options options, final String errorMessage) {
		final HelpFormatter formatter = new HelpFormatter();
		logger.error(errorMessage);
		formatter.printHelp("MinHashJaccardSimilarity", options);
		System.exit(-1);
	}

	public static void main(String[] args) {

		final long seed = System.currentTimeMillis();

		logger.info("Starting");

		/**
		 * Parse CLI
		 */

		final CommandLineParser parser = new DefaultParser();

		final Options options = new Options();
		options.addOption(PARAM_DEST_CSV_FILE, "dest", true, "destination csv file");
		options.addOption(PARAM_NUM_DOCUMENTS, "num-docs", true, "number of documents");
		options.addOption(PARAM_DOCUMENT_SIZE, "doc-size", true, "Size of each document");

		String destFile = null;
		int numDocuments = DEFAULT_NUM_DOCUMENTS;
		int documentSize = DEFAULT_DOCUMENT_SIZE;
		try {
			final CommandLine line = parser.parse(options, args);

			if (line.hasOption(PARAM_DEST_CSV_FILE)) {
				destFile = line.getOptionValue(PARAM_DEST_CSV_FILE).strip();
			} else {
				cliError(options, "Missing destination file");
			}

			if (line.hasOption(PARAM_NUM_DOCUMENTS)) {
				numDocuments = Integer.parseUnsignedInt(line.getOptionValue(PARAM_NUM_DOCUMENTS).strip());
			}

			if (line.hasOption(PARAM_DOCUMENT_SIZE)) {
				documentSize = Integer.parseUnsignedInt(line.getOptionValue(PARAM_DOCUMENT_SIZE).strip());
			}
		} catch (ParseException exp) {
			cliError(options, "Unexpected exception:" + exp.getMessage());
		}

		final MinHashJaccardSimilarity minHashJaccardSimilarity = new MinHashJaccardSimilarity();
		final List<Integer> dictionnary = minHashJaccardSimilarity.generateDictionnary();

		/**
		 * Generate documents
		 */
		logger.info("Generating {} documents and hashes", numDocuments);
		final List<Set<Integer>> documents = new ArrayList<>();
		for (int i = 0; i < numDocuments; i++) {
			final Set<Integer> document = minHashJaccardSimilarity.generateDocument(dictionnary, documentSize);
			documents.add(document);

			final Collection<Set<Integer>> variations = minHashJaccardSimilarity.generateVariations(document);
			documents.addAll(variations);
		}

		logger.info("Generated a total of {} documents", documents.size());

		final Random random = new Random(seed);
		final LCGHashGenerator lcgHashGenerator = new LCGHashGenerator(random);
		final GroupHashFunctionsFactory grouphashFunctionsFactory = new LCGGroupHashFunctionsFactory(lcgHashGenerator);

		final int numBins = NUM_BINS;
		final double binWidth = BINS_WIDTH;
		final int[] hashSizes = { 10 };

		double[][][] stats = new double[hashSizes.length][numBins][2];
		int statsIndex = 0;
		for (final int hashSize : hashSizes) {

			logger.info("Processing data with a MinHash of size {}", hashSize);

			// tag::hash[]
			final MinHash minHash = new MinHash(hashSize, grouphashFunctionsFactory);

			final int[][] hashes = new int[documents.size()][hashSize];

			for (int i = 0; i < documents.size(); i++) {
				final Set<Integer> document = documents.get(i);
				final int[] documentArr = document.stream().mapToInt(Integer::intValue).toArray();
				final int[] hash = minHash.hash(documentArr);
				hashes[i] = hash;
			}
			// end::hash[]

			int[] binsSimilarity = new int[numBins];
			ArrayList<ArrayList<Double>> binsHashSimilarity = new ArrayList<>(numBins);
			for (int i = 0; i < numBins; i++) {
				binsHashSimilarity.add(new ArrayList<>());
			}

			// tag::pairs[]
			for (int i = 0; i < documents.size(); i++) {
				for (int j = 0; j < documents.size(); j++) {
					final Set<Integer> document1 = documents.get(i);
					final int[] hash1 = hashes[i];

					final Set<Integer> document2 = documents.get(j);
					final int[] hash2 = hashes[j];

					final double jaccardSimilarity = JaccardMeasure.jaccardSimilarity(document1, document2);
					final double hashSimilarity = JaccardMeasure.jaccardSimilarityHash(hash1, hash2);

					final int binIdx = (int) (jaccardSimilarity / binWidth);
					binsHashSimilarity.get(binIdx).add(hashSimilarity);
					binsSimilarity[binIdx] += 1;
				}
			}
			// end::pairs[]

			for (int i = 0; i < numBins; i++) {
				final double sum = binsHashSimilarity.get(i).stream().mapToDouble(t -> t).sum();
				final double mean = sum / binsSimilarity[i];

				final double stdDev = Math.sqrt(
						binsHashSimilarity.get(i).stream().mapToDouble(t -> t).map(t -> (t - mean) * (t - mean)).sum()
								/ binsSimilarity[i]);

				stats[statsIndex][i][0] = mean;
				stats[statsIndex][i][1] = stdDev;
			}

			statsIndex++;
		}

		/**
		 * Dump results in a CSV
		 */
		logger.info("Generating csv file: {}", destFile);
		try {
			FileUtils.forceMkdirParent(new File(destFile));
			final String[] headers = new String[1 + hashSizes.length * 2];
			headers[0] = "Similarity";
			for (int b = 0; b < hashSizes.length; b++) {
				headers[b * 2 + 1] = "Hash size - " + hashSizes[b];
				headers[b * 2 + 2] = "Standard Deviation - " + hashSizes[b];
			}

			final CSVPrinter csvPrinter = CSVFormat.DEFAULT.withHeader(headers).print(Paths.get(destFile),
					StandardCharsets.UTF_8);

			for (int i = 0; i < NUM_BINS; i++) {
				final List<Double> row = new ArrayList<>();
				row.add((double) i * BINS_WIDTH);
				for (int j = 0; j < stats.length; j++) {
					row.add(stats[j][i][0]);
					row.add(stats[j][i][1]);
				}
				csvPrinter.printRecord(row);
			}
			csvPrinter.close(true);
		} catch (IOException e) {
			logger.error("Could not output csv file", e);
			System.exit(-1);
		}
	}
}