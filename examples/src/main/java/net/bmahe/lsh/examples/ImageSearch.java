package net.bmahe.lsh.examples;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.stream.IntStream;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.FixedSizeList;
import org.eclipse.collections.api.map.ImmutableMap;
import org.eclipse.collections.api.tuple.Pair;
import org.eclipse.collections.impl.collector.Collectors2;
import org.eclipse.collections.impl.tuple.Tuples;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.bmahe.lsh.core.LSH;
import net.bmahe.lsh.core.RandomHyperPlanesHash;
import net.bmahe.lsh.core.hashfunctions.GroupHashFunctionsFactory;
import net.bmahe.lsh.core.hashfunctions.LCGGroupHashFunctionsFactory;
import net.bmahe.lsh.core.hashfunctions.LCGHashGenerator;

public class ImageSearch {
	final static public Logger logger = LogManager.getLogger(ImageSearch.class);

	private static final String IMAGE_SEARCH = "image_search";

	public static class HashInfo {
		private final int[] hash;

		public HashInfo(final int[] _hash) {
			Objects.requireNonNull(_hash);

			this.hash = _hash;
		}

		public int[] getHash() {
			return hash;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + Arrays.hashCode(hash);
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			HashInfo other = (HashInfo) obj;
			if (!Arrays.equals(hash, other.hash))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "HashInfo [hash=" + Arrays.toString(hash) + "]";
		}
	}

	public static RealVector toRealVector(final float[] vector) {
		Objects.requireNonNull(vector);
		Validate.isTrue(vector.length > 0);

		final ArrayRealVector realVector = new ArrayRealVector(vector.length);
		for (int i = 0; i < vector.length; i++) {
			float j = vector[i];
			realVector.addToEntry(i, (double) j);
		}

		return realVector;
	}

	// tag::ImageVectorClosestMatchComparator[]
	public static class ImageVectorClosestMatchComparator implements Comparator<ImageVector> {

		private final ImageVector reference;

		private final RealVector referenceRealVector;

		public ImageVectorClosestMatchComparator(final ImageVector _reference) {
			Objects.requireNonNull(_reference);
			Objects.requireNonNull(_reference.getVector());

			this.reference = _reference;

			this.referenceRealVector = toRealVector(this.reference.getVector());
		}

		@Override
		public int compare(ImageVector iv1, ImageVector iv2) {
			if (iv1.getFilename()
					.equals(iv2.getFilename())) {
				return 0;
			}

			final RealVector realVector1 = toRealVector(iv1.getVector());
			final RealVector realVector2 = toRealVector(iv2.getVector());

			final double iv1ReferenceCos = realVector1.cosine(referenceRealVector);
			final double iv2ReferenceCos = realVector2.cosine(referenceRealVector);

			if (iv1ReferenceCos > iv2ReferenceCos) {
				return -1;
			} else if (iv1ReferenceCos < iv2ReferenceCos) {
				return 1;
			}

			return 0;
		}
	}
	// end::ImageVectorClosestMatchComparator[]

	public static void main(String[] args) {
		logger.info("Instantiating object mapper");
		final ObjectMapperUtils objectMapperUtils = new ObjectMapperUtils();
		final ObjectMapper objectMapper = objectMapperUtils.createObjectMapper();

		Validate.isTrue(args.length == 5);

		final String indexFilename = args[0];
		Validate.notBlank(indexFilename);

		final String statsFilename = args[1];
		Validate.notBlank(statsFilename);

		final String picturesDestDirectoryName = args[2];
		Validate.notBlank(picturesDestDirectoryName);

		final String picturesStatsFilename = args[3];
		Validate.notBlank(picturesStatsFilename);

		final String timingStatsFilename = args[4];
		Validate.notBlank(timingStatsFilename);

		try {
			logger.info("Loading data from {}", indexFilename);
			final Set<ImageVector> imageVectors = objectMapper.readValue(new File(indexFilename),
					new TypeReference<Set<ImageVector>>() {
					});

			Objects.requireNonNull(imageVectors);
			Validate.isTrue(imageVectors.size() > 10,
					"Dataset does not contain enough data. It needs more than 10 images");

			logger.info("Found {} entries", imageVectors.size());

			logger.info("Instantiating LSH");
			// tag::lsh_init[]
			final Random random = new Random();
			final LCGHashGenerator lcgHashGenerator = new LCGHashGenerator(random);
			final GroupHashFunctionsFactory grouphashFunctionsFactory = new LCGGroupHashFunctionsFactory(lcgHashGenerator);

			final RandomHyperPlanesHash randomHyperPlanesHash = new RandomHyperPlanesHash(512, 2000, random);
			final int numBands = 10;
			final int numBuckets = 7919;
			final LSH lsh = new LSH(grouphashFunctionsFactory, randomHyperPlanesHash, numBands, numBuckets);
			// end::lsh_init[]

			logger.info("Generating index");
			// tag::compute_hashes[]
			/**
			 * Parallelize the hash computation for each embedding vector
			 */
			final ImmutableMap<String, int[]> imageVector2Hash = imageVectors.parallelStream()
					.map(imageVector -> {
						final float[] vector = imageVector.getVector();
						final int[] hash = lsh.hash(vector);

						return Tuples.pair(imageVector.getFilename(), hash);
					})
					.collect(Collectors2.toImmutableMap(Pair::getOne, Pair::getTwo));
			// end::compute_hashes[]

			// tag::populate_index[]
			/**
			 * band -> bucket -> set of ImageVector
			 */

			// Pre-allocate the bands
			final FixedSizeList<Map<Integer, Set<ImageVector>>> indexImages = Lists.fixedSize
					.ofAll(IntStream.range(0, lsh.getNumBands())
							.boxed()
							.map(i -> new HashMap<Integer, Set<ImageVector>>())
							.toList());

			for (final ImageVector imageVector : imageVectors) {
				final String filename = imageVector.getFilename();

				final int[] hash = imageVector2Hash.get(filename);

				for (int i = 0; i < hash.length; i++) {
					final int j = hash[i];

					final Map<Integer, Set<ImageVector>> bucket2ImageVectors = indexImages.get(i);
					final Set<ImageVector> indexedImageVectors = bucket2ImageVectors.computeIfAbsent(j,
							k -> new HashSet<>());

					indexedImageVectors.add(imageVector);
				}
			}
			// end::populate_index[]

			logger.info("Selecting random image vector");
			/**
			 * Get a random image
			 */
			// tag::select_random_image[]
			final Random random2 = new Random(); // don't want to interfere with the generated numbers for the LSH
			final ImageVector randomImageVector = imageVectors.stream()
					.skip(random2.nextInt(imageVectors.size()))
					.findFirst()
					.get();
			logger.info("Random image vector selected: {} - {}",
					randomImageVector.getFilename(),
					randomImageVector.getLabel());

			logger.info("Doing a naive search by comparing each images with the selected one");
			final long startTimeNaiveSearchMs = System.currentTimeMillis();
			final List<ImageVector> naiveCandidates = imageVectors.stream()
					.sorted(new ImageVectorClosestMatchComparator(randomImageVector))
					.toList();
			final long endTimeNaiveSearchMs = System.currentTimeMillis();
			final long durationNaiveSearchMs = endTimeNaiveSearchMs - startTimeNaiveSearchMs;
			logger.info("Finished naive search. It took {}ms to compare {} images",
					durationNaiveSearchMs,
					imageVectors.size());

			logger.info("Computing hash of the selected random image");
			final int[] randomImageVectorHash = lsh.hash(randomImageVector.getVector());
			// end::select_random_image[]

			logger.info("Quering for all the hashs with at least a matching band");
			// tag::find_closest_image[]
			final long startTimeLSHSearchMs = System.currentTimeMillis();
			final Set<ImageVector> matches = new HashSet<>();
			for (int band = 0; band < randomImageVectorHash.length; band++) {
				final int bucket = randomImageVectorHash[band];

				final Map<Integer, Set<ImageVector>> bucket2Vectors = indexImages.get(band);

				final Set<ImageVector> indexedImageVectors = bucket2Vectors.getOrDefault(bucket, Collections.emptySet());

				matches.addAll(indexedImageVectors);
			}
			final List<ImageVector> orderedMatches = matches.stream()
					.sorted(new ImageVectorClosestMatchComparator(randomImageVector))
					.toList();
			final long endTimeLSHSearchMs = System.currentTimeMillis();
			final long durationLSHSearchMs = endTimeLSHSearchMs - startTimeLSHSearchMs;
			logger.info("Found {} matches among {} total images in {}ms",
					matches.size(),
					imageVectors.size(),
					durationLSHSearchMs);
			// end::find_closest_image[]

			/**
			 * Dump timing stats in a CSV
			 */
			logger.info("Generating csv file for global timing stats: {}", timingStatsFilename);
			try {
				FileUtils.forceMkdirParent(new File(timingStatsFilename));
				final String[] headers = { "Naive search time (ms)", "LSH search time (ms)",
						"Relative performance change (%)" };

				final CSVPrinter csvPrinter = CSVFormat.DEFAULT.withHeader(headers)
						.print(Paths.get(timingStatsFilename), StandardCharsets.UTF_8);

				final String naiveSearchTimingStr = Long.toString(durationNaiveSearchMs);
				final String lshSearchTimingStr = Long.toString(durationLSHSearchMs);
				final String percentChange = String.format("%d %%",
						(int) (((float) durationLSHSearchMs - durationNaiveSearchMs) / durationNaiveSearchMs * 100.0d));

				final String[] performanceStats = { naiveSearchTimingStr, lshSearchTimingStr, percentChange };
				csvPrinter.printRecord((Object[]) performanceStats);
				csvPrinter.close(true);
			} catch (IOException e) {
				logger.error("Could not output csv file: " + timingStatsFilename, e);
				System.exit(-1);
			}

			/**
			 * Dump results in a CSV
			 */
			logger.info("Generating csv file for global stats: {}", statsFilename);
			try {
				FileUtils.forceMkdirParent(new File(statsFilename));
				final String[] headers = { "Number of matches", "Total number of images", "Percent selected" };

				final CSVPrinter csvPrinter = CSVFormat.DEFAULT.withHeader(headers)
						.print(Paths.get(statsFilename), StandardCharsets.UTF_8);

				final String matchStr = Integer.toString(orderedMatches.size());
				final String totalImageStr = Integer.toString(imageVectors.size());
				final String percentMatched = String.format("%d %%",
						(int) ((float) orderedMatches.size() / imageVectors.size() * 100.0d));

				final String[] matchStats = { matchStr, totalImageStr, percentMatched };
				csvPrinter.printRecord((Object[]) matchStats);
				csvPrinter.close(true);
			} catch (IOException e) {
				logger.error("Could not output csv file: " + statsFilename, e);
				System.exit(-1);
			}

			/**
			 * Log top 5 k matches
			 */

			FileUtils.forceMkdirParent(Paths.get(picturesStatsFilename)
					.toFile());
			final String[] headers = { "Cosine of the angle", "Picture" };

			final CSVPrinter csvPrinter = CSVFormat.DEFAULT.withHeader(headers)
					.print(Paths.get(picturesStatsFilename), StandardCharsets.UTF_8);

			final RealVector randomIVRealV = toRealVector(randomImageVector.getVector());
			orderedMatches.stream()
					.limit(5)
					.forEach(iv -> {

						final RealVector ivRealVector = toRealVector(iv.getVector());
						final double cosine = randomIVRealV.cosine(ivRealVector);

						logger.info("{} - {}: {}", iv.getFilename(), iv.getLabel(), cosine);

						try {
							final String cosDistStr = String.format("%.3f", cosine);

							final String destFilename = "image::"
									+ Paths.get(IMAGE_SEARCH, new File(iv.getFilename()).getName())
											.toString()
									+ "[]";

							FileUtils.copyFileToDirectory(new File(iv.getFilename()),
									Paths.get(picturesDestDirectoryName, IMAGE_SEARCH)
											.toFile());
							csvPrinter.printRecord(Arrays.asList(cosDistStr, destFilename));
						} catch (IOException e) {
							logger.error("Error with file {}", iv.getFilename(), e);
							System.exit(-1);
						}
					});

			csvPrinter.close(true);
		} catch (IOException e) {
			logger.error("Error", e);
			System.exit(-1);
		}
	}
}