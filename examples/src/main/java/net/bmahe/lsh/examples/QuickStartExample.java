package net.bmahe.lsh.examples;

import java.util.Random;
import java.util.Set;

// tag::lshminhash_import[]
import net.bmahe.lsh.core.LSH;
import net.bmahe.lsh.core.LSHMinHashFactory;
// end::lshminhash_import[]

//tag::lshrandomhyperplanes_import[]
import net.bmahe.lsh.core.LSH;
import net.bmahe.lsh.core.LSHRandomHyperPlanesHashFactory;
//end::lshrandomhyperplanes_import[]
import net.bmahe.lsh.core.UnorderedLSH;

public class QuickStartExample {

	public static void lshMinHash() {

		// tag::lshminhash_build[]
		final int numBands = 5; // <1>
		final int numBuckets = 10; // <2>
		final Random seed = new Random(); // <3>

		final UnorderedLSH lshMinHash = LSHMinHashFactory.build(numBands, numBuckets, seed);
		// end::lshminhash_build[]

		// tag::lshminhash_hashintarray[]
		final int[] docArray = { 1, 150, 200, 6 };
		final int[] hashDocArray = lshMinHash.hash(docArray);
		// end::lshminhash_hashintarray[]

		// tag::lshminhash_hashintset[]
		final Set<Integer> docSet = Set.of(300, 6, 200, 150);
		final int[] hashDocSet = lshMinHash.hash(docSet);
		// end::lshminhash_hashintset[]
	}

	public static void lshRandomHyperPlanes() {

		// tag::lshrandomhyperplanes_build[]
		final int inputSize = 4; // <1>
		final int hashSize = 3; // <2>
		final int numBands = 5; // <3>
		final int numBuckets = 10; // <4>
		final Random seed = new Random(); // <5>

		final LSH lshRandomHyperPlanes = LSHRandomHyperPlanesHashFactory.build(inputSize, hashSize, numBands, numBuckets,
				seed);
		// end::lshrandomhyperplanes_build[]

		// tag::lshrandomhyperplanes_hashintarray[]
		final int[] docArray = { 1, 150, 200, 6 };
		final int[] hashDocArray = lshRandomHyperPlanes.hash(docArray);
		// end::lshrandomhyperplanes_hashintarray[]

		// tag::lshrandomhyperplanes_hashdoublearray[]
		final double[] docDoubleArray = { 1.0d, 150.0d, 200.0d, 6.0d };
		final int[] hashDoubleDocArray = lshRandomHyperPlanes.hash(docDoubleArray);
		// end::lshrandomhyperplanes_hashdoublearray[]
	}
}