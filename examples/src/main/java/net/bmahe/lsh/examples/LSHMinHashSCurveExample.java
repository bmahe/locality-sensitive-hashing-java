package net.bmahe.lsh.examples;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.bmahe.lsh.core.JaccardMeasure;
import net.bmahe.lsh.core.LSH;
import net.bmahe.lsh.core.LSHMinHashFactory;

public class LSHMinHashSCurveExample {

	final static public Logger logger = LogManager.getLogger(LSHMinHashSCurveExample.class);

	final static public int DEFAULT_DICTIONNARY_SIZE = 100;
	final static public int DEFAULT_DOCUMENT_SIZE = 30;
	final static public int DEFAULT_NUM_DOCUMENTS = 100;
	final static public int DEFAULT_NUM_BUCKETS = 15;
	final static public int[] DEFAULT_BANDS = { 2, 4, 6, 8, 10 };

	final static private String PARAM_DEST_CSV_FILE = "d";
	final static private String PARAM_BANDS = "b";
	final static private String PARAM_NUM_DOCUMENTS = "n";
	final static private String PARAM_NUM_BUCKETS = "k";
	final static private String PARAM_DOCUMENT_SIZE = "s";

	/**
	 * Bins are used to build histograms for the s-curve graph.
	 * 
	 * 
	 * Number of bins is equal to ((max - min) / width) + 1
	 * 
	 * The extra one is due to when the similarity is 1.0
	 */
	final static private double BINS_WIDTH = 0.05;
	final static private int NUM_BINS = 1 + (int) (1. / BINS_WIDTH);

	final private Random random = new Random();

	private List<Integer> generateDictionnary() {
		return Stream.iterate(0, t -> t < DEFAULT_DICTIONNARY_SIZE, e -> e + 1).collect(Collectors.toList());
	}

	// tag::generate_document[]
	private Set<Integer> generateDocument(final List<Integer> dictionnary, final int size) {

		final Set<Integer> document = new HashSet<>();

		while (document.size() < size) { // <1>
			final int index = random.nextInt(dictionnary.size()); // <2>

			final Integer element = dictionnary.get(index);
			document.add(element);
		}

		return document;
	}
	// end::generate_document[]

	// tag::generate_document_variations[]
	private Collection<Set<Integer>> generateVariations(final Set<Integer> document) {
		final Set<Set<Integer>> variations = new HashSet<>();

		final int documentSize = document.size();

		int[] copyDocument = new int[documentSize];
		for (int j = 0; j < documentSize; j++) { // <1>
			final Iterator<Integer> documentIt = document.iterator();
			/**
			 * Re-init
			 */
			for (int k = 0; k < documentSize; k++) {
				copyDocument[k] = documentIt.next();
			}

			/**
			 * Remove up to j elements
			 */
			for (int k = 0; k < j; k++) {
				copyDocument[k] = -1; // <2>
			}

			/**
			 * Re-serialize
			 */
			final Set<Integer> newDoc = new HashSet<>();
			for (int k = 0; k < documentSize; k++) {
				if (copyDocument[k] != -1) {
					newDoc.add(copyDocument[k]);
				}
			}

			variations.add(newDoc); // <3>
		}

		return variations;
	}
	// end::generate_document_variations[]

	public double[] process(final long seed, final int numBands, final int numBuckets, final int numBins,
			final double binWidth, final List<Set<Integer>> documents) {
		logger.info("Processing documents for {} bands and {} buckets", numBands, numBuckets);

		// tag::hash[]
		final LSH lshMinHash = LSHMinHashFactory.build(numBands, numBuckets, seed);
		final int[][] hashes = new int[documents.size()][numBands];

		for (int i = 0; i < documents.size(); i++) {
			final Set<Integer> document = documents.get(i);
			final int[] documentArr = document.stream().mapToInt(Integer::intValue).toArray();
			final int[] hash = lshMinHash.hash(documentArr);
			hashes[i] = hash;
		}
		// end::hash[]

		// tag::pairs[]
		int[] binsSimilarity = new int[numBins];
		int[] binsHasBandMatch = new int[numBins];
		for (int i = 0; i < documents.size(); i++) {
			for (int j = 0; j < documents.size(); j++) {
				final Set<Integer> document1 = documents.get(i);
				final int[] hash1 = hashes[i];

				final Set<Integer> document2 = documents.get(j);
				final int[] hash2 = hashes[j];

				final double jaccardSimilarity = JaccardMeasure.jaccardSimilarity(document1, document2); // <1>

				boolean hasBandMatch = false;
				for (int band = 0; band < numBands && hasBandMatch == false; band++) {
					if (hash1[band] == hash2[band]) { // <2>
						hasBandMatch = true;
					}
				}

				final int binIdx = (int) (jaccardSimilarity / binWidth);
				binsSimilarity[binIdx] += 1; // <3>
				if (hasBandMatch) {
					binsHasBandMatch[binIdx] += 1; // <4>
				}
			}
		}
		// end::pairs[]

		double[] stats = new double[numBins];
		for (int i = 0; i < numBins; i++) {
			stats[i] = binsHasBandMatch[i] > 0 ? (double) binsHasBandMatch[i] / binsSimilarity[i] : 0.0;
		}

		return stats;
	}

	public static void cliError(final Options options, final String errorMessage) {
		final HelpFormatter formatter = new HelpFormatter();
		logger.error(errorMessage);
		formatter.printHelp("LSHMinHashSCurveExample", options);
		System.exit(-1);
	}

	public static void main(String[] args) {

		final long seed = System.currentTimeMillis();

		logger.info("Starting");

		/**
		 * Parse CLI
		 */

		final CommandLineParser parser = new DefaultParser();

		final Options options = new Options();
		options.addOption(PARAM_DEST_CSV_FILE, "dest", true, "destination csv file");
		options.addOption(PARAM_BANDS, "bands", true, "number of bands");
		options.addOption(PARAM_NUM_DOCUMENTS, "num-docs", true, "number of documents");
		options.addOption(PARAM_NUM_BUCKETS, "num-buckets", true, "number of buckets");
		options.addOption(PARAM_DOCUMENT_SIZE, "doc-size", true, "Size of each document");

		String destFile = null;
		int[] bands = DEFAULT_BANDS;
		int numDocuments = DEFAULT_NUM_DOCUMENTS;
		int numBuckets = DEFAULT_NUM_BUCKETS;
		int documentSize = DEFAULT_DOCUMENT_SIZE;
		try {
			final CommandLine line = parser.parse(options, args);

			if (line.hasOption(PARAM_DEST_CSV_FILE)) {
				destFile = line.getOptionValue(PARAM_DEST_CSV_FILE).strip();
			} else {
				cliError(options, "Missing destination file");
			}

			if (line.hasOption(PARAM_BANDS)) {
				final String[] bandsStr = line.getOptionValues(PARAM_BANDS);
				bands = Arrays.asList(bandsStr).stream().map(String::strip).mapToInt(Integer::parseInt).toArray();
			}

			if (line.hasOption(PARAM_NUM_DOCUMENTS)) {
				numDocuments = Integer.parseUnsignedInt(line.getOptionValue(PARAM_NUM_DOCUMENTS).strip());
			}

			if (line.hasOption(PARAM_NUM_BUCKETS)) {
				numBuckets = Integer.parseUnsignedInt(line.getOptionValue(PARAM_NUM_BUCKETS).strip());
			}

			if (line.hasOption(PARAM_DOCUMENT_SIZE)) {
				documentSize = Integer.parseUnsignedInt(line.getOptionValue(PARAM_DOCUMENT_SIZE).strip());
			}
		} catch (ParseException exp) {
			cliError(options, "Unexpected exception:" + exp.getMessage());
		}

		/**
		 * Initialize
		 */

		final LSHMinHashSCurveExample lshMinHashSCurveExample = new LSHMinHashSCurveExample();

		final List<Integer> dictionnary = lshMinHashSCurveExample.generateDictionnary();

		/**
		 * Generate documents
		 */
		logger.info("Generating {} documents and hashes", numDocuments);
		final List<Set<Integer>> documents = new ArrayList<>();
		for (int i = 0; i < numDocuments; i++) {
			final Set<Integer> document = lshMinHashSCurveExample.generateDocument(dictionnary, documentSize);
			documents.add(document);

			final Collection<Set<Integer>> variations = lshMinHashSCurveExample.generateVariations(document);
			documents.addAll(variations);

		}

		/**
		 * Hash and compare
		 */
		logger.info("Comparing hashes for bands {} and {} buckets", bands, numBuckets);

		final double[][] stats = new double[bands.length][NUM_BINS];
		for (int i = 0; i < bands.length; i++) {
			int numBand = bands[i];
			stats[i] = lshMinHashSCurveExample.process(seed, numBand, numBuckets, NUM_BINS, BINS_WIDTH, documents);
		}

		/**
		 * Dump results in a CSV
		 */
		logger.info("Generating csv file: {}", destFile);
		try {
			FileUtils.forceMkdirParent(new File(destFile));
			final String[] headers = new String[1 + bands.length];
			headers[0] = "Similarity";
			for (int b = 0; b < bands.length; b++) {
				headers[b + 1] = bands[b] + " bands";
			}

			final CSVPrinter csvPrinter = CSVFormat.DEFAULT.withHeader(headers).print(Paths.get(destFile),
					StandardCharsets.UTF_8);

			for (int i = 0; i < NUM_BINS; i++) {
				final List<Double> row = new ArrayList<>();
				row.add((double) i * BINS_WIDTH);
				for (int j = 0; j < stats.length; j++) {
					row.add(stats[j][i]);
				}
				csvPrinter.printRecord(row);
			}
			csvPrinter.close(true);
		} catch (IOException e) {
			logger.error("Could not output csv file", e);
			System.exit(-1);
		}
	}
}