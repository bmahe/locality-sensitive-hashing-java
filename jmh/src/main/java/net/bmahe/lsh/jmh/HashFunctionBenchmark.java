package net.bmahe.lsh.jmh;

import java.util.Random;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

import net.bmahe.lsh.core.hashfunctions.HashFunction;
import net.bmahe.lsh.core.hashfunctions.LCGHashGenerator;

public class HashFunctionBenchmark {

	@State(Scope.Thread)
	public static class ThreadState {
		final Random random = new Random();
		final LCGHashGenerator lcgUniversalHashGenerator = new LCGHashGenerator(random);
		final int idx = 4_000_011;

		public final HashFunction hashFunction;

		public ThreadState() {
			hashFunction = lcgUniversalHashGenerator.generate();
		}
	}

	@Benchmark
	@BenchmarkMode(value = { Mode.AverageTime, Mode.Throughput })
	public HashFunction hashFunnctionGeneration(final ThreadState threadState) {
		return threadState.lcgUniversalHashGenerator.generate();
	}

	@Benchmark
	@BenchmarkMode(value = { Mode.AverageTime, Mode.Throughput })
	public Integer hashFunnction(final ThreadState threadState) {
		return threadState.hashFunction.apply(threadState.idx);
	}

}