package net.bmahe.lsh.jmh;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

import net.bmahe.lsh.core.MinHash;
import net.bmahe.lsh.core.hashfunctions.GroupHashFunctionsFactory;
import net.bmahe.lsh.core.hashfunctions.LCGGroupHashFunctionsFactory;
import net.bmahe.lsh.core.hashfunctions.LCGHashGenerator;

public class MinHashBenchmark {
	@State(Scope.Thread)
	public static class ThreadState {
		final int MAX_DOC = 10_000_000;
		final int docSize = 1000;

		final Random random = new Random();
		final LCGHashGenerator lcgUniversalHashGenerator = new LCGHashGenerator(random);

		Set<Integer> dictionnary;

		final MinHash minHash;
		public Set<Integer> document;
		public final Map<Integer, Integer> element2Index;

		final Set<Integer> idxSet;
		final int[] idxArray;

		public ThreadState() {

			dictionnary = new HashSet<>();
			for (int i = 0; i < MAX_DOC; i++) {
				dictionnary.add(i);
			}

			final GroupHashFunctionsFactory groupHashFunctionsFactory = new LCGGroupHashFunctionsFactory(
					lcgUniversalHashGenerator);
			minHash = new MinHash(100, groupHashFunctionsFactory);

			document = new HashSet<>();
			for (int i = 0; i < docSize; i++) {
				document.add(random.nextInt(MAX_DOC));
			}

			element2Index = new HashMap<>();
			for (Integer element : dictionnary) {
				element2Index.putIfAbsent(element, element2Index.size());
			}

			idxArray = new int[document.size()];
			int i = 0;
			for (Integer integer : document) {
				idxArray[i] = element2Index.get(integer);
				i++;
			}

			idxSet = new HashSet<>();
			for (Integer integer : document) {
				idxSet.add(element2Index.get(integer));
			}
		}
	}

	@Benchmark
	@BenchmarkMode(value = { Mode.AverageTime, Mode.Throughput })
	public int[] minHash(final ThreadState threadState) {

		return threadState.minHash.hash(threadState.idxArray);
	}
}